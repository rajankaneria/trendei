-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 29, 2015 at 07:09 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `trendei`
--

-- --------------------------------------------------------

--
-- Table structure for table `campaign`
--

CREATE TABLE IF NOT EXISTS `campaign` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `campaign_name` varchar(100) NOT NULL,
  `product` varchar(100) NOT NULL,
  `budget` int(11) NOT NULL,
  `campaign_time` double NOT NULL,
  `keywords` text NOT NULL,
  `age_group` varchar(50) NOT NULL,
  `gender` varchar(50) NOT NULL,
  `industry_type` varchar(100) NOT NULL,
  `preferred_platform` varchar(100) NOT NULL,
  `time_line` text NOT NULL,
  `user_mem_id` int(11) NOT NULL,
  `unique_link` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `campaign`
--

INSERT INTO `campaign` (`id`, `campaign_name`, `product`, `budget`, `campaign_time`, `keywords`, `age_group`, `gender`, `industry_type`, `preferred_platform`, `time_line`, `user_mem_id`, `unique_link`) VALUES
(4, 'Promotion for Appstyr', '2', 5000, 14, 'tech, social, media', '13-17,18-29', 'male', 'Software', 'facebook', '', 3, ''),
(5, 'Rajan Test campaign', '5', 5000, 1, 'social, nature, fitness, tag1, tag2, tag3', '13-17', 'male', 'Software', 'twitter,facebook', '', 9, '');

-- --------------------------------------------------------

--
-- Table structure for table `campaign_type`
--

CREATE TABLE IF NOT EXISTS `campaign_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `campaign_type_name` varchar(50) NOT NULL,
  `campaign_type_value` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `industry`
--

CREATE TABLE IF NOT EXISTS `industry` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `values` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE IF NOT EXISTS `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `launch_date` varchar(50) NOT NULL,
  `user_mem_id` int(11) NOT NULL,
  `app_image` varchar(50) NOT NULL,
  `product_link` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `name`, `description`, `launch_date`, `user_mem_id`, `app_image`, `product_link`) VALUES
(1, 'Test App one', 'This is the test application which will be used only for testing purpose. There is no relation between this application and any other application with similar name found out in the market', '15-Jan-2015', 2, '', ''),
(2, 'Appstyr (ios App)', 'Discover & share apps with friends the same you do music, photos, memes etc.', '14-Feb-2015', 3, '', ''),
(3, 'Test Application', 'this is test application description', '20-June-2015', 3, '', ''),
(4, 'chttah', 'tst app', 'slakdj alsk jd', 3, '1421842683_3.jpg', ''),
(5, 'Testing App', 'This is test description. This is test description. This is test description. This is test description.', '1-Feb-2015', 9, '1422462859_9.jpg', 'http://www.google.com/');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fullname` varchar(100) NOT NULL,
  `user_type` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `country` varchar(50) NOT NULL,
  `state` varchar(50) NOT NULL,
  `city` varchar(50) NOT NULL,
  `interests` text NOT NULL,
  `followers` int(11) NOT NULL,
  `data_created` double NOT NULL,
  `account_balance` int(11) NOT NULL,
  `total_earnings` int(11) NOT NULL,
  `Q_score` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `fullname`, `user_type`, `email`, `password`, `address`, `country`, `state`, `city`, `interests`, `followers`, `data_created`, `account_balance`, `total_earnings`, `Q_score`) VALUES
(2, 'Rajan Kaneria', 'influencer', 'rajan.kaneria@gmail.com', '7815696ecbf1c96e6894b779456d330e', '', '', '', '', '', 0, 0, 0, 0, 0),
(3, 'Sameer Kaneria', 'marketer', 'sameerkaneria@gmail.com', '7815696ecbf1c96e6894b779456d330e', '', '', '', '', '', 0, 0, 0, 0, 0),
(7, 'Test User', 'marketer', 'test@test.test', '7815696ecbf1c96e6894b779456d330e', '', '', '', '', '', 0, 0, 0, 0, 0),
(8, 'Sameer Kaneria', 'influencer', 'sameerkaneria@gmail.com', '7815696ecbf1c96e6894b779456d330e', '', '', '', '', '', 0, 0, 0, 0, 0),
(9, 'Rajan Kaneria', 'marketer', 'rajan.kaneria@gmail.com', '7815696ecbf1c96e6894b779456d330e', '', '', '', '', '', 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_type`
--

CREATE TABLE IF NOT EXISTS `user_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_type_name` varchar(50) NOT NULL,
  `user_type_value` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
