// influencer signup
var site_url = $("#siteURL").val();
var base_url = $("#baseURL").val();
function influencer_login()
{
    var email = $("#influencerEmail").val();
    var password = $("#influencerPassword").val();

    //add a column named fullname in users table
    var userData = {
        "user_type" : "influencer",
        "password" : password,
        "email" :email
    };
    // here goes the ajax call to the backend
    //alert(site_url+"/influencer/user_login");
    $.post(site_url+"/influencer/user_login",{userData:userData},function(data){
        if(data=="invalid")
        {
            alert("Please check your login details");
        }
        else if(data=="success")
        {
            window.location.href=site_url+"/influencer/dashboard";
        }
    })

}
function marketer_login()
{
    var email = $("#marketerEmail").val();
    var password = $("#marketerPassword").val();

    //add a column named fullname in users table
    var userData = {
        "user_type" : "marketer",
        "password" : password,
        "email" :email
    };
    // here goes the ajax call to the backend
    //alert(site_url+"/influencer/user_login");
    $.post(site_url+"/marketer/user_login",{userData:userData},function(data){
        if(data=="invalid")
        {
            alert("Please check your login details");
        }
        else if(data=="success")
        {
            window.location.href=site_url+"/marketer/dashboard";
        }
    })

}
function influencer_signup()
{
    var fullname = $("#influencerName").val();
    var email = $("#influencerEmail").val();
    var password = $("#influencerPassword").val();
    var password2 = $("#influencerPassword2").val();
    var submit_flag = 1;

    //check if user has checked the agreement
    if (!$("#influencerAgreement").is(':checked'))
    {
        alert("You have to agree our terms and condition to sign up!");
        submit_flag = 0;
    }
    else if(password!=password2)//check if password matches
    {
        alert("Password mismatched!");
        submit_flag = 0;
    }
    if(submit_flag==1)
    {
        //add a column named fullname in users table
        var userData = {
            "user_type" : "influencer",
            "fullname" : fullname,
            "password" : password,
            "email" :email
        };
       // here goes the ajax call to the backend
        //alert(site_url+"/influencer/user_signup");
        $.post(site_url+"/influencer/user_signup",{userData:userData},function(data){
            if(data=="invalid")
            {
                alert("Email already registered");
            }
            else if(data=="success")
            {
                window.location.href=site_url+"/influencer/dashboard";
            }
        })
    }


}


function marketer_signup()
{
    var fullname = $("#marketerName").val();
    var email = $("#marketerEmail").val();
    var password = $("#marketerPassword").val();
    var password2 = $("#marketerPassword2").val();
    var submit_flag = 1;

    //check if user has checked the agreement
    if (!$("#marketerAgreement").is(':checked'))
    {
        alert("You have to agree our terms and condition to sign up!");
        submit_flag = 0;
    }
    else if(password!=password2)//check if password matches
    {
        alert("Password mismatched!");
        submit_flag = 0;
    }
    if(submit_flag==1)
    {
        //add a column named fullname in users table
        var userData = {
            "user_type" : "marketer",
            "fullname" : fullname,
            "password" : password,
            "email" :email
        };
       // here goes the ajax call to the backend
        //alert(site_url+"/influencer/user_signup");
        $.post(site_url+"/marketer/user_signup",{userData:userData},function(data){
            if(data=="invalid")
            {
                alert("Email already registered");
            }
            else if(data=="success")
            {
                window.location.href=site_url+"/marketer/dashboard";
            }
        })
    }


}


function marketer_campaign_launch()
{
    var campaignname = $("#campaignName").val();
    var reservation = $("#reservation").val();
    var campaignapp = $("#campaignapp").val();
    var budget = $("#campaignbudget").val();
    var keywords = $("#keywords").val();
    var industry = $("#industry").val();
    var gender = $("#gender").val();
    var preferred_platform = [];
    $("[name=preferredsmedia]:checked").each(function(){
        preferred_platform.push($(this).val());
    });
    preferred_platform = preferred_platform.join();
    var age_group = [];
    $("[name=agegroup]:checked").each(function(){
        age_group.push($(this).val());
    });
    age_group = age_group.join();
   var submit_flag = 1;

    // if(campaignapp) {

    //     alert("Campaign app cannot be empty, please select an app!");
    //     submit_flag = 0;
    // }
    // else if(agegroup.length){
    //     alert("Age group can't be empty, please check at lease one age group");
    //     submit_flag = 0;
    // }
    // else if(preferredsmedia.length){
    //     alert("Preffered media can't be empty, please check at lease one platform");
    //     submit_flag = 0;
    // }

    if(submit_flag==1)
    {
        //add a column named fullname in users table
        var campaignData = {
            "campaign_name" : campaignname,
            "product" : campaignapp,
            "campaign_time" : reservation,
            "keywords" :keywords,
            "age_group" : age_group,
            "gender" : gender,
            "industry_type" : industry,
            "preferred_platform" :preferred_platform,
            "budget" :budget,
            "time_line" :reservation
        };
       // here goes the ajax call to the backend
        //alert(site_url+"/influencer/user_signup");
        $("#campaignLaunchBtn").html("Launching...");
        $.post(site_url+"/marketer/marketer_create_campaign",{campaignData:campaignData},function(data){
            if(data=="invalid")
            {
                alert("Campaign already exist");
            }
            else if(data=="success")
            {
                $("#campaignLaunchBtn").html("Redirecting...");
                window.location.href=site_url+"/marketer/campaignmanagement";
            }
        })
    }


}
function save_application()
{
    var applicationData = {
        "name" : $("#appName").val(),
        "description" : $("#appDescription").val(),
        "product_link" : $("#appLink").val()
    };
    $("#addAppBtn").html("Adding...");
    $.post(site_url+"/marketer/add_product",{applicationData:applicationData},function(data){
        $("#addAppBtn").html("Redirecting...");
        location.reload();
    });
}
function view_app(app_id)
{
    $("#viewAppModal .modal-body").html("Loading...");
    $("#viewAppModal").modal("show");
    $.post(site_url+"/marketer/view_product",{product_id:app_id},function(data){
        $("#viewAppModal .modal-body").html(data);
    });
}
function delete_product(product_id)
{
    $("#product-"+product_id).fadeOut(300);
    $.post(site_url+"/marketer/delete_product",{product_id:product_id},function(data){
        $("#product-"+product_id).remove();
    });
}
function edit_product(product_id)
{
    $("#editAppModal .modal-body").html("Loading...");
    $("#editAppModal").modal("show");
    $.post(site_url+"/marketer/edit_product",{product_id:product_id},function(data){
        $("#editAppModal .modal-body").html(data);
        init_app_photo_upload();
    });
}
function save_product()
{
    $("#save_product_btn").html("Saving...");
    var product_id = $("#product_id").val();
    var productData = {
        "name" : $("#edit_product_name").val(),
        "description" : $("#edit_product_description").val(),
        "product_link" : $("#edit_product_link").val()
    }
    $.post(site_url+"/marketer/update_product",{product_id:product_id,productData:productData},function(data){
        $("#save_product_btn").html("Save");
        $("#editAppModal").modal("hide");
        $("#product-"+product_id+" .product-name-value").html($("#edit_product_name").val());
        $("#product-"+product_id+" .product-link-value").html($("#edit_product_link").val());
    });
}
function init_app_photo_upload()
{
    $("#editAppImageBtn").unbind("click");
    $("#editAppImageInput").unbind("change");
    $("#editAppImageBtn").on("click",function(){ $("#editAppImageInput").click(); });
    $('#editAppImageInput').on('change', function(){
        //$("#newPortfolioImageLoader").fadeIn(300);
        var formData = new FormData($('#editAppImageForm')[0]);
        $.ajax({
            url: site_url+'/marketer/update_app_image',
            type: 'POST',
            data: formData,
            contentType: false,
            processData: false,
            success: function(data){
                $("#editAppImageBtn").attr("src",base_url+"/uploads/products/"+data);
                //$("#newPortfolioImageLoader").fadeOut(300);
            }
        });
    });
}


function view_campaign(campaign_id)
{
    $("#viewCampaignModal .modal-body").html("Loading...");
    $("#viewCampaignModal").modal("show");
    $.post(site_url+"/marketer/view_campaign",{campaign_id:campaign_id},function(data){
        $("#viewCampaignModal .modal-body").html(data);
    });
}
function edit_campaign(campaign_id)
{
    $("#editCampaignModal .modal-body").html("Loading...");
    $("#editCampaignModal").modal("show");
    $.post(site_url+"/marketer/edit_campaign",{campaign_id:campaign_id},function(data){
        $("#editCampaignModal .modal-body").html(data);
    });
}
function save_campaign()
{
    var campaignname = $("#campaignName").val();
    var reservation = $("#reservation").val();
    var campaignapp = $("#campaignapp").val();
    var budget = $("#campaignbudget").val();
    var keywords = $("#keywords").val();
    var industry = $("#industry").val();
    var gender = $("#gender:checked").val();
    var preferred_platform = [];
    $("[name=preferredsmedia]:checked").each(function(){
        preferred_platform.push($(this).val());
    });
    preferred_platform = preferred_platform.join();
    var age_group = [];
    $("[name=agegroup]:checked").each(function(){
        age_group.push($(this).val());
    });
    age_group = age_group.join();
    var submit_flag = 1;

    // if(campaignapp) {

    //     alert("Campaign app cannot be empty, please select an app!");
    //     submit_flag = 0;
    // }
    // else if(agegroup.length){
    //     alert("Age group can't be empty, please check at lease one age group");
    //     submit_flag = 0;
    // }
    // else if(preferredsmedia.length){
    //     alert("Preffered media can't be empty, please check at lease one platform");
    //     submit_flag = 0;
    // }

    if(submit_flag==1)
    {
        var campaign_id = $("#campaign_id").val();
        //add a column named fullname in users table
        var campaignData = {
            "campaign_name" : campaignname,
            "product" : campaignapp,
            "campaign_time" : reservation,
            "keywords" :keywords,
            "age_group" : age_group,
            "gender" : gender,
            "industry_type" : industry,
            "preferred_platform" :preferred_platform,
            "budget" :budget,
            "time_line" :reservation
        };
        // here goes the ajax call to the backend
        //alert(site_url+"/influencer/user_signup");
        $("#save_campaign_btn").html("Saving...");
        $.post(site_url+"/marketer/marketer_update_campaign",{campaignData:campaignData,campaign_id:campaign_id},function(data){
                $("#save_campaign_btn").html("Reloading Campaign...");
                location.reload();
        })
    }


}
function delete_campaign(campaign_id)
{
    $("#campaign-"+campaign_id).fadeOut(300);
    $.post(site_url+"/marketer/delete_campaign",{campaign_id:campaign_id},function(data){
        $("#campaign-"+campaign_id).remove();
    });
}
