<?php

class product_model extends CI_Model
{
    function product_create($product_input)
    {
        $user_mem_id = $this->session->userdata('user_mem_id');
        $product_input["user_mem_id"] = $user_mem_id;
        if($this->session->userdata("temp_product_img")!="")
        {
            $product_input["app_image"] = $this->session->userdata("temp_product_img");
        }
        $this->db->insert('product', $product_input);
        return "success";
    }
    function get_product($user_mem_id)
    {
        $campaign_query = $this->db->get_where("product",array("user_mem_id"=>$user_mem_id));
        $result = $campaign_query->result_array();
        return $result;
    }
    function get_all_product()
    {
        $campaign_query = $this->db->get("product");
        $result = $campaign_query->result_array();
        return $result;
    }
    function get_row($product_id)
    {
        $product_query = $this->db->get_where("product",array("id"=>$product_id));
        $result = $product_query->row_array();
        return $result;
    }
    function delete_row($product_id)
    {
        /* delete product picture */
        $this->db->where("id",$product_id);
        $query = $this->db->get('product');
        $result = $query->row_array();
        unlink('./uploads/products/'.$result["app_image"]);

        $this->db->delete('product', array('id' => $product_id));
    }
    function update_row($product_id,$productData)
    {
        $this->db->where("id",$product_id);
        $productData["app_image"] = $this->session->userdata("temp_product_img");
        $this->db->update('product', $productData);
    }
}