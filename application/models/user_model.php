<?php

class user_model extends CI_Model
{

    function create_influencer($user_input)
    {

        $email_query = $this->db->get_where("user", array("email" => $user_input["email"],"user_type"=>"influencer"));
        if ($email_query->num_rows == 1)
        {
            return "invalid";
        }
        else
        {
            $insert_query = $this->db->insert('user', $user_input);
            $query = $this->db->get_where("user",$user_input);
            $user_row = $query->row_array();

            $data = array(
                "user_mem_id" => $user_row["id"],
                "email" => $user_input["email"],
                "fullname" =>$user_input["fullname"]
            );
            $this->session->set_userdata($data);
            return "success";
        }

    }
    function login($user_input)
    {

        $email_query = $this->db->get_where("user",$user_input);
        if ($email_query->num_rows != 1)
        {
            return "invalid";
        }
        else
        {
            $query = $this->db->get_where("user",$user_input);
            $user_row = $query->row_array();
            $data = array(
                "user_mem_id" => $user_row["id"],
                "email" => $user_row["email"],
                "fullname" =>$user_row["fullname"]
            );
            $this->session->set_userdata($data);
            return "success";
        }

    }

    function create_marketer($user_input)
    {

        $email_query = $this->db->get_where("user", array("email" => $user_input["email"],"user_type"=>"marketer"));
        if ($email_query->num_rows == 1)
        {
            return "invalid";
        }
        else
        {
            $insert_query = $this->db->insert('user', $user_input);
            $query = $this->db->get_where("user",$user_input);
            $user_row = $query->row_array();

            $data = array(
                "user_mem_id" => $user_row["id"],
                "email" => $user_input["email"],
                "fullname" =>$user_input["fullname"]
            );
            $this->session->set_userdata($data);
            return "success";
        }

    }
    function switch_check($user_id)
    {
        $query = $this->db->get_where("user",array("id"=>$user_id));
        $user_row = $query->row_array();

        $user_email = $user_row["email"];
        $user_type = $user_row["user_type"];

        $switch_to_type = ($user_type=="marketer" ? "influencer" : "marketer");

        $email_query = $this->db->get_where("user", array("email" => $user_email,"user_type"=>$switch_to_type));
        if ($email_query->num_rows == 1)
        {
            $switch_user_row = $email_query->row_array();
            $data = array(
                "user_mem_id" => $switch_user_row["id"],
                "email" => $switch_user_row["email"],
                "fullname" =>$switch_user_row["fullname"]
            );
            $this->session->set_userdata($data);

        }
        else
        {
            $user_input = array(
              "fullname" => $user_row["fullname"],
              "user_type" => $switch_to_type,
              "email" => $user_row["email"],
              "password" => $user_row["password"],
            );
            $insert_query = $this->db->insert('user', $user_input);
            $user_id = $this->db->insert_id();

            $data = array(
                "user_mem_id" => $user_id,
                "email" => $user_row["email"],
                "fullname" =>$user_row["fullname"]
            );
            $this->session->set_userdata($data);
        }
        return "$switch_to_type/dashboard";
    }


}