<?php

class campaign_model extends CI_Model
{
    function campaign_create($campaign_input)
    {
        $user_mem_id = $this->session->userdata('user_mem_id');
        $campaign_input["user_mem_id"] = $user_mem_id;
        $this->db->insert('campaign', $campaign_input);
        return "success";
    }
    function get_campaign($user_mem_id)
    {

        $this->db->where("user_mem_id",$user_mem_id);
        $campaign_query = $this->db->get("campaign");
        $result = $campaign_query->result_array();
        $this->load->model("product_model");
        foreach($result as $key => $result_row)
        {
            $product_id = $result_row["product"];
            $result[$key]["product"] = $this->product_model->get_row($product_id);
        }
        return $result;


    }
    function get_all_campaign()
    {
        $campaign_query = $this->db->get("campaign");
        $result = $campaign_query->result_array();

        $this->load->model("product_model");
        foreach($result as $key=>$result_row)
        {
            $product_id = $result_row["product"];
            $result[$key]["product"] = $this->product_model->get_row($product_id);
        }
        return $result;

    }
    function get_row($campaign_id)
    {
        $product_query = $this->db->get_where("campaign",array("id"=>$campaign_id));
        $result = $product_query->row_array();
        $this->load->model("product_model");
        $result["product"] = $this->product_model->get_row($result["product"]);
        return $result;
    }
    function campaign_update($campaign_id,$campaignData)
    {
        $this->db->where("id",$campaign_id);
        $this->db->update('campaign', $campaignData);
    }
    function delete_row($campaign_id)
    {
       $this->db->delete('campaign', array('id' => $campaign_id));
    }

}