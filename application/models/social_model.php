<?php

class social_model extends CI_Model
{
    function get_tweets($url) {
        $json_string = $this->file_get_contents_curl('http://urls.api.twitter.com/1/urls/count.json?url=' . $url);
        $json = json_decode($json_string, true);
        return isset($json['count'])?intval($json['count']):0;
    }

    function get_fb($url) {
        $json_string = $this->file_get_contents_curl('http://api.facebook.com/restserver.php?method=links.getStats&format=json&urls='.$url);
        $json = json_decode($json_string, true);
        //var_dump($json);
        return isset($json[0]['share_count'])?intval($json[0]['share_count']):0;
    }
    function get_plusones($url)  {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, "https://clients6.google.com/rpc");
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_POSTFIELDS, '[{"method":"pos.plusones.get","id":"p","params":{"nolog":true,"id":"'.rawurldecode($url).'","source":"widget","userId":"@viewer","groupId":"@self"},"jsonrpc":"2.0","key":"p","apiVersion":"v1"}]');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
        $curl_results = curl_exec ($curl);
        curl_close ($curl);
        $json = json_decode($curl_results, true);
        return isset($json[0]['result']['metadata']['globalCounts']['count'])?intval( $json[0]['result']['metadata']['globalCounts']['count'] ):0;
    }
    function get_stumble($url) {
        $json_string = $this->file_get_contents_curl('http://www.stumbleupon.com/services/1.01/badge.getinfo?url='.$url);
        $json = json_decode($json_string, true);
        return isset($json['result']['views'])?intval($json['result']['views']):0;
    }

    function get_pinterest($url) {
        $return_data = $this->file_get_contents_curl('http://api.pinterest.com/v1/urls/count.json?callback=receiveCount&url='.$url);
        $json_string = str_replace("receiveCount(","",$return_data);
        $json_string = str_replace(")","",$json_string);
        //$json_string = preg_replace('/^receiveCount((.*))$/', "\1", $return_data);
        $json = json_decode($json_string, true);
        return isset($json['count'])?intval($json['count']):0;
    }
    private function file_get_contents_curl($url){
        $ch=curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
        curl_setopt($ch, CURLOPT_FAILONERROR, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        $cont = curl_exec($ch);
        if(curl_error($ch))
        {
            die(curl_error($ch));
        }
        return $cont;
    }

}