<?php

class photo_upload extends CI_Model{
	
	function upload_picture($upload_data)
	{
		$upload_folder = $upload_data["upload_folder"]; // eg: uploads/
		$upload_input = $upload_data["upload_input"];
		$upload_type = $upload_data["upload_type"];
		$userid = $this->session->userdata("user_mem_id");
		$this->load->helper('url');
		$config['upload_path'] = "./".$upload_folder;
		$config['allowed_types'] = 'gif|jpg|png';
		$this->load->library('upload', $config);
		
		if($uploaded_file = $this->upload->do_upload($upload_input))
		{

			$data = $this->upload->data();
			$file_name = $data["file_name"];
			$new_file_name = time()."_".$userid.$data["file_ext"];
			$data = array(
				"temp_".$upload_type."_img" => $new_file_name
			);
			$this->session->set_userdata($data);

			/*************** Image crop start *******************/
			list($width, $height, $type, $attr) = getimagesize('./'.$upload_folder.$file_name);
			if($width>$height){
				$crop_size = $height;
			}
			else {
				$crop_size = $width;
			}
			$crop_config['image_library'] = 'GD2';
			$crop_config['source_image']	= './'.$upload_folder.$file_name;
			$crop_config['create_thumb'] = FALSE;
			$crop_config['maintain_ratio'] = FALSE;
			$crop_config['x_axis'] = '0';
			$crop_config['y_axis'] = '0';
			$crop_config['width']	= $crop_size;
			$crop_config['height']	= $crop_size;
			$crop_config['new_image']	= $new_file_name;
			$this->load->library('image_lib');
			$this->image_lib->initialize($crop_config);
			$this->image_lib->crop();
			/*************** Image crop end *******************/

			/*************** Image resize start *******************/
			$resize_config['image_library'] = 'gd2';
			$resize_config['source_image']	= './'.$upload_folder.$new_file_name;
			$resize_config['create_thumb'] = FALSE;
			$resize_config['maintain_ratio'] = TRUE;
			$resize_config['width']	= 500;
			$resize_config['height']	= 500;
			$resize_config['new_image']	= $new_file_name;
			$this->load->library('image_lib');
			$this->image_lib->initialize($resize_config);
			$this->image_lib->resize();
			//rename('./'.$upload_folder.$file_name,'./'.$upload_folder."original_".$new_file_name);
			//unlink('./'.$upload_folder.$file_name);
			/*************** Image resize end *******************/
			return $new_file_name;
		}
		else
		{
			return "failed";
		}		
	}
	
}
