<?php $this->load->view("includes/marketer_nav",$header_data); ?>
<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">

    <!-- Main content -->
    <section class="content-header">
        <div class="row"><br />
            <div class="headline text-center .headliner">
                New Campaign
                <h4 class="subheader">List your apps for free in just 3 easy steps</h4>

            </div><!-- /.row -->
        </div>

        <div class="panel-heading content"></div>
    </section>

    <div class="panel-body">
        <div class="modal-body">
            <!-- <form id="createCampaignForm" method="post" action="campaigncreate.php"> -->
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <label class="control-label">Campaign Name</label>
                            <input type="text" class="form-control" id="campaignName" name="campaignname" placeholder="Company Name" required/>
                        </div>

                        <div class="col-md-6">
                            <label class="control-label">Launch Date - End Date</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" name="reservation" class="form-control pull-right" id="reservation" required/>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6 selectContainer">
                            <label class="control-label">Campaign App(choose which app you want to promote)</label>
                            <select class="form-control" id="campaignapp" name="campaignapp">
                                <option value="">Choose an app</option>
                                <?php foreach($main_content["app_list"] as $app_row){ ?>
                                <option value='<?php echo $app_row["id"]; ?>'><?php echo $app_row["name"]; ?></option>
                                <?php } ?>
                            </select>

                        </div>

                        <div class="col-md-4">
                            <label class="control-label">OR</label>
                            <a href="#" class="btn btn-danger pull-right top-buffer" onclick="$('#newAppModal').modal('show');"><i class="glyphicon glyphicon-plus" ></i> Add New Product</a>
                        </div>

                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <div class="col-md-4">
                            <label class="control-label">Campaign Budget</label>
                            <div class="input-group">
                                <span class="input-group-addon">$</span>
                                <input type="text" class="form-control" id="campaignbudget" name="campaignbudget" required>
                                <span class="input-group-addon">.00</span>
                            </div>
                        </div>

                        <div class="col-md-8">
                            <label class="control-label">Relevance Keywords & phrases (ex:social, nature, fitness, etc)</label>
                            <input type="text" class="form-control" id="keywords" name="keywords" placeholder="Use comma (,) to separate multiple entries" required/>
                        </div>

                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <div class="col-md-4">
                            <label class="control-label">Age Group</label>
                            <div class="checkbox">
                                <label><input type="checkbox"  name="agegroup" value="13-17">13-17</label>
                            </div>
                            <div class="checkbox">
                                <label><input type="checkbox"  name="agegroup" value="18-29">18-29</label>
                            </div>
                            <div class="checkbox">
                                <label><input type="checkbox"  name="agegroup" value="30-55">30-55</label>
                            </div>
                            <div class="checkbox">
                                <label><input type="checkbox"  name="agegroup" value="55+">55+</label>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <label class="control-label">Gender</label>
                            <div class="radio">
                                <label><input type="radio" name="gender" id="gender" value="male" > Male </label>
                            </div>
                            <div class="radio">
                                <label><input type="radio" name="gender" id="gender" value="female" > Female </label>
                            </div>
                            <div class="radio">
                                <label><input type="radio" name="gender" id="gender" value="both" > Both </label>
                            </div>
                        </div>

                        <div class="col-md-4 selectContainer">
                            <label class="control-label">Industry</label>
                            <select class="form-control" id="industry" name="industry">
                                <option value="">Select App Industry</option>
                                <option value="Software">Software</option>
                                <option value="Business">Business</option>
                                <option value="Sport">Sport</option>
                                <option value="Others">Others</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group" >
                    <label class="control-label">Preferred Network</label>
                    <div class="row">
                        <dl class="dl-horizontal">
                            <dd>
                                <label class="ui-checkbox"><input  name="preferredsmedia" type="checkbox" value="twitter" ><span><a href="javascript:;" class="btn-icon-round btn-icon-round-sm bg-twitter"><i class="fa fa-twitter"></i></a></span></label>
                                <label class="ui-checkbox"><input  name="preferredsmedia" type="checkbox" value="facebook" ><span><a href="javascript:;" class="btn-icon-round btn-icon-round-sm bg-facebook"><i class="fa fa-facebook"></i></a></span></label>
                                <label class="ui-checkbox"><input  name="preferredsmedia" type="checkbox" value="googleplus"><span><a href="javascript:;" class="btn-icon-round btn-icon-round-sm bg-google-plus"><i class="fa fa-google-plus"></i></a></span></label>
                            </dd>
                        </dl>
                    </div>
                </div>

<!--                 <div class="form-group">
                    <a href="<?php echo site_url("marketer/dashboard"); ?>" type="button" class="btn btn-danger">Cancel1</a>
                    <button type="button" class="btn btn-success" onclick="marketer_campaign_launch();">Launch1</button>
                </div> -->

                 <div class="footer">
                    <a href="<?php echo site_url("marketer/dashboard"); ?>" type="button" class="btn btn-danger">Cancel</a>
                    <button id="campaignLaunchBtn" type="button" class="btn btn-success" onclick="marketer_campaign_launch();">Launch</button>
                 </div>
            <!-- </form> -->
        </div>
    </div>
    </section>

    </div>


    <!--

        <div class="row boxrow">
            <div class="form-group col-md-10">
                <label class="control-label newcamplabel">Tags & Keywords</label>
                <div ng-app="plunker" ng-controller="MainCtrl" >
                    <tags-input ng-model="tags"></tags-input>
                </div>
            </div>
        </div><br /><br />

        <div class="form-group">
            <div class="row boxrow">
                <div class="col-md-10">
                    <label class="control-label"> Tags & Keywords (Ex: social, nature, fitness, etc)</label>
                    <input type="text" class="form-control keywords" name="keywords" placeholder="Use comma to separate multiple entries"/>
                </div>
            </div>
        </div><br />

        <div class="form-group">
            <div class="row boxrow">
                <div class="col-md-3">
                    <label class="control-label">Age Group</label>
                    <div class="checkbox">
                        <label><input type="checkbox" name="agegroup[]" value="13-17">13-17</label>
                    </div>
                    <div class="checkbox">
                        <label><input type="checkbox" name="agegroup[]" value="18-29">18-29</label>
                    </div>
                    <div class="checkbox">
                        <label><input type="checkbox" name="agegroup[]" value="30-55">30-55</label>
                    </div>
                    <div class="checkbox">
                        <label><input type="checkbox" name="agegroup[]" value="55+">55+</label>
                    </div>
                </div>

                <div class="col-md-3">
                    <label class="control-label">Gender</label>
                    <div class="radio">
                        <label><input type="radio" name="gender" id="optionsRadios1" value="male" checked> Male </label>
                    </div>
                    <div class="radio">
                        <label><input type="radio" name="gender" id="optionsRadios1" value="female" checked> Female </label>
                    </div>
                    <div class="radio">
                        <label><input type="radio" name="gender" id="optionsRadios1" value="both" checked> Both </label>
                    </div>
                </div>

                <div class="col-md-3 selectContainer">
                    <label class="control-label">Industry</label>
                    <select class="form-control" name="industry">
                        <option value="">Select App Industry</option>
                        <option value="Software">Software</option>
                        <option value="Business">Business</option>
                        <option value="Sport">Sport</option>
                        <option value="Entertainment">Entertainment</option>
                        <option value="Others">Others</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="row boxrow">
                <label class="control-label">Preferred Network</label>
                <dl class="dl-horizontal">
                    <dd>
                        <label class="ui-checkbox"><input name="preferredsmedia[]" type="checkbox" value="twitter" ><span><a href="javascript:;" class="btn-icon-round btn-icon-round-sm bg-twitter"><i class="fa fa-twitter"></i></a></span></label>
                        <label class="ui-checkbox"><input name="preferredsmedia[]" type="checkbox" value="facebook" checked><span><a href="javascript:;" class="btn-icon-round btn-icon-round-sm bg-facebook"><i class="fa fa-facebook"></i></a></span></label>
                        <label class="ui-checkbox"><input name="preferredsmedia[]" type="checkbox" value="googleplus"><span><a href="javascript:;" class="btn-icon-round btn-icon-round-sm bg-google-plus"><i class="fa fa-google-plus"></i></a></span></label>
                    </dd>
                </dl>
            </div>
        </div>

        <div class="form-group launchbtn">
            <a href="campaignmanagement.php" type="button" class="btn btn-danger btn-lg">Cancel</a>
            <button type="submit" class="btn btn-success btn-lg"> Continue</button>
        </div>
        </form>
        </div>
        </div>
        </section>

        </div> -->
    </section><!-- /.content -->
</aside><!-- /.right-side -->
</div><!-- ./wrapper -->




<script>
    $(function(){
        $("#newAppImageBtn").on("click",function(){ $("#newAppImageInput").click(); });
        $('#newAppImageInput').on('change', function(){
            //$("#newPortfolioImageLoader").fadeIn(300);
            var formData = new FormData($('#newAppImageForm')[0]);
            $.ajax({
                url: site_url+'/marketer/upload_app_image',
                type: 'POST',
                data: formData,
                contentType: false,
                processData: false,
                success: function(data){
                    $("#newAppImageBtn").attr("src",base_url+"/uploads/products/"+data);
                    //$("#newPortfolioImageLoader").fadeOut(300);
                }
            });
        });
    });
</script>