<div class="form-box" id="login-box">
    <div class="header">Signup as a Creative</div>

        <div class="body bg-gray">
            <div class="form-group">
                <input type="text" id="marketerName" name="name" class="form-control" placeholder="Full name"/>
            </div>
            <div class="form-group">
                <input type="email" id="marketerEmail" class="form-control" name="marketerInputEmail1" placeholder="Enter email" required>
            </div>
            <div class="form-group">
                <input type="password" id="marketerPassword" name="password" class="form-control" placeholder="Password"/>
            </div>
            <div class="form-group">
                <input type="password" id="marketerPassword2" name="password2" class="form-control" placeholder="Retype password"/>
            </div>
            <div class="form-group">
                I agree to terms and conditions  <input id="marketerAgreement" type=checkbox name=agree value='yes'/>
            </div>
        </div>

         <div class="footer">

            <button type="button" class="btn bg-olive btn-block" onclick="marketer_signup();">Sign me up</button>

            Already signed up? <a href="<?php echo site_url("marketer/login"); ?>" class="text-center"> Login</a>
        </div>

    <div class="margin text-center">
        <span>-</span>
    </div>
</div>