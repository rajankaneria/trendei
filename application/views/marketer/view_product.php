<div class="product-container">
    <div class="image-frame-left">
        <img src="<?php echo base_url()."/uploads/products/".$app_image; ?>"/>
    </div>
    <div class="product-info-right">
        <div class="product-name"><?php echo $name; ?></div>
        <div class="product-description"><?php echo $description; ?></div>
        <div class="product-link"><a href="<?php echo $product_link; ?>" target="_blank">View Website</a></div>
    </div>
</div>