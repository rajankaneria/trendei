<?php if(count($campaign_array)!=0){

    ?>
<?php foreach($campaign_array as $campaign_row){ ?>
    <tr id="campaign-<?php echo $campaign_row["id"]; ?>">
        <td><?php echo $campaign_row["campaign_name"]; ?></td>
        <td><?php echo $campaign_row["product"]["name"]; ?></td>
        <td><?php echo $campaign_row["time_line"]; ?></td>
        <td width=250>
            <a class="fa fa-eye" style="margin-right: 2em" href="#" onclick="view_campaign('<?php echo $campaign_row["id"]; ?>')"></a>
            &nbsp;
            <a class="fa fa-edit" style="margin-right: 2em" href="#" onclick="edit_campaign('<?php echo $campaign_row["id"]; ?>')"></a>
            &nbsp;
            <a class="fa fa-remove" href="#" onclick="delete_campaign('<?php echo $campaign_row["id"]; ?>')"></a>
        </td>
    </tr>
<?php } ?>
<?php }else{ ?>
    <tr>
        <td colspan="4" style="text-align: center; margin:50px;">No campaign added yet!</td>
    </tr>
<?php } ?>