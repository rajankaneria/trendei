<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="row"><br />
            <div class="headline text-center .headliner">
                <p><a href="<?php echo site_url("marketer/campaigncreate"); ?>" type="button" class="btn btn-primary btn-lg btn newcampaign">Launch New Campaign</a> </p>
            </div><br /><br />
        </div>
        <div class="panel-heading content"><strong><span class="fa fa-group"></span> Total Reach</strong></div>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Main row -->
        <!-- gray graph -->

        <!-- <div class="panel-body">
             <div morris-chart="" data-data="mainData" data-type="area" data-xkey="month" data-ykeys="[&quot;xbox&quot;, &quot;will&quot;, &quot;playstation&quot;]" data-labels="[&quot;xbox&quot;, &quot;will&quot;, &quot;playstation&quot;]" data-line-colors="[&quot;#6A55C2&quot;,&quot;#E94B3B&quot;,&quot;#2EC1CC&quot;]" data-line-width="0" data-behave-like-line="true" data-point-size="0" class="ng-isolate-scope" style="position: relative;"><svg height="342" version="1.1" width="998" xmlns="http://www.w3.org/2000/svg" style="overflow: hidden; position: relative;"><desc style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Created with Raphaël 2.1.0</desc><defs style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></defs><text x="55.90625" y="306.984375" text-anchor="end" font="10px &quot;Arial&quot;" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-style: normal; font-variant: normal; font-weight: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: sans-serif;" font-size="12px" font-family="sans-serif" font-weight="normal"><tspan dy="4.1640625" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">0</tspan></text><path fill="none" stroke="#aaaaaa" d="M68.40625,306.984375H973" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="55.90625" y="236.48828125" text-anchor="end" font="10px &quot;Arial&quot;" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-style: normal; font-variant: normal; font-weight: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: sans-serif;" font-size="12px" font-family="sans-serif" font-weight="normal"><tspan dy="4.16015625" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">10,000</tspan></text><path fill="none" stroke="#aaaaaa" d="M68.40625,236.48828125H973" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="55.90625" y="165.9921875" text-anchor="end" font="10px &quot;Arial&quot;" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-style: normal; font-variant: normal; font-weight: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: sans-serif;" font-size="12px" font-family="sans-serif" font-weight="normal"><tspan dy="4.15625" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">20,000</tspan></text><path fill="none" stroke="#aaaaaa" d="M68.40625,165.9921875H973" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="55.90625" y="95.49609375" text-anchor="end" font="10px &quot;Arial&quot;" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-style: normal; font-variant: normal; font-weight: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: sans-serif;" font-size="12px" font-family="sans-serif" font-weight="normal"><tspan dy="4.16015625" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">30,000</tspan></text><path fill="none" stroke="#aaaaaa" d="M68.40625,95.49609375H973" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="55.90625" y="25" text-anchor="end" font="10px &quot;Arial&quot;" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-style: normal; font-variant: normal; font-weight: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: sans-serif;" font-size="12px" font-family="sans-serif" font-weight="normal"><tspan dy="4.1640625" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">40,000</tspan></text><path fill="none" stroke="#aaaaaa" d="M68.40625,25H973" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="973" y="319.484375" text-anchor="middle" font="10px &quot;Arial&quot;" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-style: normal; font-variant: normal; font-weight: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: sans-serif;" font-size="12px" font-family="sans-serif" font-weight="normal" transform="matrix(1,0,0,1,0,7.0078)"><tspan dy="4.1640625" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">2013-09</tspan></text><text x="857.5793603155548" y="319.484375" text-anchor="middle" font="10px &quot;Arial&quot;" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-style: normal; font-variant: normal; font-weight: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: sans-serif;" font-size="12px" font-family="sans-serif" font-weight="normal" transform="matrix(1,0,0,1,0,7.0078)"><tspan dy="4.1640625" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">2013-08</tspan></text><text x="742.1587206311096" y="319.484375" text-anchor="middle" font="10px &quot;Arial&quot;" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-style: normal; font-variant: normal; font-weight: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: sans-serif;" font-size="12px" font-family="sans-serif" font-weight="normal" transform="matrix(1,0,0,1,0,7.0078)"><tspan dy="4.1640625" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">2013-07</tspan></text><text x="630.4613273880981" y="319.484375" text-anchor="middle" font="10px &quot;Arial&quot;" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-style: normal; font-variant: normal; font-weight: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: sans-serif;" font-size="12px" font-family="sans-serif" font-weight="normal" transform="matrix(1,0,0,1,0,7.0078)"><tspan dy="4.1640625" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">2013-06</tspan></text><text x="515.0406877036529" y="319.484375" text-anchor="middle" font="10px &quot;Arial&quot;" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-style: normal; font-variant: normal; font-weight: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: sans-serif;" font-size="12px" font-family="sans-serif" font-weight="normal" transform="matrix(1,0,0,1,0,7.0078)"><tspan dy="4.1640625" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">2013-05</tspan></text><text x="403.3432944606414" y="319.484375" text-anchor="middle" font="10px &quot;Arial&quot;" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-style: normal; font-variant: normal; font-weight: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: sans-serif;" font-size="12px" font-family="sans-serif" font-weight="normal" transform="matrix(1,0,0,1,0,7.0078)"><tspan dy="4.1640625" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">2013-04</tspan></text><text x="288.0777900445893" y="319.484375" text-anchor="middle" font="10px &quot;Arial&quot;" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-style: normal; font-variant: normal; font-weight: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: sans-serif;" font-size="12px" font-family="sans-serif" font-weight="normal" transform="matrix(1,0,0,1,0,7.0078)"><tspan dy="4.1640625" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">2013-03</tspan></text><text x="183.8268896844452" y="319.484375" text-anchor="middle" font="10px &quot;Arial&quot;" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-style: normal; font-variant: normal; font-weight: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: sans-serif;" font-size="12px" font-family="sans-serif" font-weight="normal" transform="matrix(1,0,0,1,0,7.0078)"><tspan dy="4.1640625" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">2013-02</tspan></text><text x="68.40625" y="319.484375" text-anchor="middle" font="10px &quot;Arial&quot;" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-style: normal; font-variant: normal; font-weight: normal; font-stretch: normal; font-size: 12px; line-height: normal; font-family: sans-serif;" font-size="12px" font-family="sans-serif" font-weight="normal" transform="matrix(1,0,0,1,0,7.0078)"><tspan dy="4.1640625" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">2013-01</tspan></text><path fill="#9082cc" stroke="none" d="M68.40625,168.81203125000002C97.2614099211113,176.5666015625,154.9717297633339,193.96559509622173,183.8268896844452,199.8303125C209.88961477448123,205.1274766066384,262.0150649545533,210.4464097651649,288.0777900445893,213.45955729166667C316.8941661486023,216.7910582026649,374.52691835662836,228.55044358054224,403.3432944606414,225.20890624999998C431.26764277139426,221.97080816387557,487.11633939290004,194.32930584016395,515.0406877036529,187.14101562500002C543.8958476247642,179.7131157359973,601.6061674669868,173.61077354422815,630.4613273880981,166.74414583333333C658.385675698851,160.0990222421448,714.2343723203568,137.618472827015,742.1587206311096,133.09401041666666C771.0138805522209,128.41873259264003,828.7242003944435,139.21542122395834,857.5793603155548,129.94518489583334C886.4345202366661,120.67494856770834,944.1448400788887,76.68538606770835,973,58.93211979166668L973,306.984375L68.40625,306.984375Z" fill-opacity="0.8" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 0.8;"></path><path fill="none" stroke="#6a55c2" d="M68.40625,168.81203125000002C97.2614099211113,176.5666015625,154.9717297633339,193.96559509622173,183.8268896844452,199.8303125C209.88961477448123,205.1274766066384,262.0150649545533,210.4464097651649,288.0777900445893,213.45955729166667C316.8941661486023,216.7910582026649,374.52691835662836,228.55044358054224,403.3432944606414,225.20890624999998C431.26764277139426,221.97080816387557,487.11633939290004,194.32930584016395,515.0406877036529,187.14101562500002C543.8958476247642,179.7131157359973,601.6061674669868,173.61077354422815,630.4613273880981,166.74414583333333C658.385675698851,160.0990222421448,714.2343723203568,137.618472827015,742.1587206311096,133.09401041666666C771.0138805522209,128.41873259264003,828.7242003944435,139.21542122395834,857.5793603155548,129.94518489583334C886.4345202366661,120.67494856770834,944.1448400788887,76.68538606770835,973,58.93211979166668" stroke-width="0" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><circle cx="68.40625" cy="168.81203125000002" r="03" fill="#6a55c2" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="183.8268896844452" cy="199.8303125" r="0" fill="#6a55c2" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="288.0777900445893" cy="213.45955729166667" r="0" fill="#6a55c2" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="403.3432944606414" cy="225.20890624999998" r="0" fill="#6a55c2" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="515.0406877036529" cy="187.14101562500002" r="0" fill="#6a55c2" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="630.4613273880981" cy="166.74414583333333" r="0" fill="#6a55c2" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="742.1587206311096" cy="133.09401041666666" r="0" fill="#6a55c2" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="857.5793603155548" cy="129.94518489583334" r="0" fill="#6a55c2" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="973" cy="58.93211979166668" r="0" fill="#6a55c2" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><path fill="#e88075" stroke="none" d="M68.40625,243.06791666666666C97.2614099211113,219.68671223958333,154.9717297633339,150.96297790872174,183.8268896844452,149.54309895833333C209.88961477448123,148.26062764830507,262.0150649545533,222.49368567800354,288.0777900445893,232.258515625C316.8941661486023,243.05504635508686,374.52691835662836,241.27612087302774,403.3432944606414,231.78854166666667C431.26764277139426,222.59465602927776,487.11633939290004,171.50475089651638,515.0406877036529,157.53265625C543.8958476247642,143.0948251152664,601.6061674669868,112.59582656676912,630.4613273880981,118.14883854166666C658.385675698851,123.52272109801912,714.2343723203568,195.565876664959,742.1587206311096,201.240234375C771.0138805522209,207.10373734204234,828.7242003944435,168.7650338541667,857.5793603155548,164.30028125C886.4345202366661,159.83552864583334,944.1448400788887,165.21673046875,973,165.52221354166667L973,306.984375L68.40625,306.984375Z" fill-opacity="0.8" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 0.8;"></path><path fill="none" stroke="#e94b3b" d="M68.40625,243.06791666666666C97.2614099211113,219.68671223958333,154.9717297633339,150.96297790872174,183.8268896844452,149.54309895833333C209.88961477448123,148.26062764830507,262.0150649545533,222.49368567800354,288.0777900445893,232.258515625C316.8941661486023,243.05504635508686,374.52691835662836,241.27612087302774,403.3432944606414,231.78854166666667C431.26764277139426,222.59465602927776,487.11633939290004,171.50475089651638,515.0406877036529,157.53265625C543.8958476247642,143.0948251152664,601.6061674669868,112.59582656676912,630.4613273880981,118.14883854166666C658.385675698851,123.52272109801912,714.2343723203568,195.565876664959,742.1587206311096,201.240234375C771.0138805522209,207.10373734204234,828.7242003944435,168.7650338541667,857.5793603155548,164.30028125C886.4345202366661,159.83552864583334,944.1448400788887,165.21673046875,973,165.52221354166667" stroke-width="0" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><circle cx="68.40625" cy="243.06791666666666" r="03" fill="#e94b3b" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="183.8268896844452" cy="149.54309895833333" r="0" fill="#e94b3b" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="288.0777900445893" cy="232.258515625" r="0" fill="#e94b3b" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="403.3432944606414" cy="231.78854166666667" r="0" fill="#e94b3b" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="515.0406877036529" cy="157.53265625" r="0" fill="#e94b3b" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="630.4613273880981" cy="118.14883854166666" r="0" fill="#e94b3b" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="742.1587206311096" cy="201.240234375" r="0" fill="#e94b3b" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="857.5793603155548" cy="164.30028125" r="0" fill="#e94b3b" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="973" cy="165.52221354166667" r="0" fill="#e94b3b" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><path fill="#5ac9d1" stroke="none" d="M68.40625,192.31072916666668C97.2614099211113,206.05746744791668,154.9717297633339,240.26002140713277,183.8268896844452,247.29768229166666C209.88961477448123,253.65427921963277,262.0150649545533,243.37680414458188,288.0777900445893,245.88776041666668C316.8941661486023,248.6640111758319,374.52691835662836,265.5823355619162,403.3432944606414,268.4465104166667C431.26764277139426,271.22202306191616,487.11633939290004,269.4057195611339,515.0406877036529,268.4465104166667C543.8958476247642,267.45532763405055,601.6061674669868,265.2425977843238,630.4613273880981,260.64494270833336C658.385675698851,256.1955990864071,714.2343723203568,234.0729232838115,742.1587206311096,232.258515625C771.0138805522209,230.3836277108948,828.7242003944435,243.56138932291668,857.5793603155548,245.88776041666668C886.4345202366661,248.21413151041668,944.1448400788887,249.6240533854167,973,250.869484375L973,306.984375L68.40625,306.984375Z" fill-opacity="0.8" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 0.8;"></path><path fill="none" stroke="#2ec1cc" d="M68.40625,192.31072916666668C97.2614099211113,206.05746744791668,154.9717297633339,240.26002140713277,183.8268896844452,247.29768229166666C209.88961477448123,253.65427921963277,262.0150649545533,243.37680414458188,288.0777900445893,245.88776041666668C316.8941661486023,248.6640111758319,374.52691835662836,265.5823355619162,403.3432944606414,268.4465104166667C431.26764277139426,271.22202306191616,487.11633939290004,269.4057195611339,515.0406877036529,268.4465104166667C543.8958476247642,267.45532763405055,601.6061674669868,265.2425977843238,630.4613273880981,260.64494270833336C658.385675698851,256.1955990864071,714.2343723203568,234.0729232838115,742.1587206311096,232.258515625C771.0138805522209,230.3836277108948,828.7242003944435,243.56138932291668,857.5793603155548,245.88776041666668C886.4345202366661,248.21413151041668,944.1448400788887,249.6240533854167,973,250.869484375" stroke-width="0" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><circle cx="68.40625" cy="192.31072916666668" r="03" fill="#2ec1cc" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="183.8268896844452" cy="247.29768229166666" r="0" fill="#2ec1cc" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="288.0777900445893" cy="245.88776041666668" r="0" fill="#2ec1cc" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="403.3432944606414" cy="268.4465104166667" r="0" fill="#2ec1cc" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="515.0406877036529" cy="268.4465104166667" r="0" fill="#2ec1cc" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="630.4613273880981" cy="260.64494270833336" r="0" fill="#2ec1cc" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="742.1587206311096" cy="232.258515625" r="0" fill="#2ec1cc" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="857.5793603155548" cy="245.88776041666668" r="0" fill="#2ec1cc" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle><circle cx="973" cy="250.869484375" r="0" fill="#2ec1cc" stroke="#ffffff" stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle></svg><div class="morris-hover morris-default-style" style="left: 6.40625px; top: 139px;"><div class="morris-hover-row-label">Total Reach</div><div class="morris-hover-point" style="color: #6A55C2">
                   Facebook:
                   34,000
             </div>
             <div class="morris-hover-point" style="color: #E94B3B">
                   Twitter:
                   20,000
             </div><div class="morris-hover-point" style="color: #2EC1CC">
                   Instagram:
                   10,000
             </div>
         </div> -->
        <!-- End gray graph -->
        <!-- Blue graph -->
        <div class="row">
            <!-- right col (We are only adding the ID to make the widgets sortable)-->
            <section class="col-lg-12 connectedSortable">
                <!-- solid sales graph -->
                <div class="box box-solid bg-red">
                    <div class="box-header">
                        <i class="fa fa-th"></i>
                        <h3 class="box-title">Total Reach</h3>
                        <div class="box-tools pull-right">
                            <button class="btn bg-red btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="box-body border-radius-none">
                        <div class="chart" id="line-chart" style="height: 250px;"></div>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </section><!-- right col -->
        </div><!-- /.row (main row) -->


        <!-- This is for the social media stat -->
        <div class="row">
            <div class="col-md-3">
                <div class="social-box facebook">
                    <i class="fa fa-facebook"></i>
                    <ul>
                        <li>
                            <strong>---</strong>
                            <span>impressions</span>
                        </li>
                        <li>
                            <strong>---</strong>
                            <span>clicks</span>
                        </li>
                        <li>
                            <strong>--</strong>
                            <span>likes</span>
                        </li>
                        <li>
                            <strong>---</strong>
                            <span>shares</span>
                        </li>
                    </ul>
                </div>
            </div><!-- ./col -->
            <div class="col-md-3">
                <div class="social-box twitter">
                    <i class="fa fa-twitter"></i>
                    <ul>
                        <li>
                            <strong>---</strong>
                            <span>impressions</span>
                        </li>
                        <li>
                            <strong>---</strong>
                            <span>clicks</span>
                        </li>
                        <li>
                            <strong>---</strong>
                            <span>likes</span>
                        </li>
                        <li>
                            <strong>---</strong>
                            <span>shares</span>
                        </li>
                    </ul>
                </div>
            </div><!-- ./col -->
            <div class="col-md-3">
                <div class="social-box instagram">
                    <i class="fa fa-instagram"></i>
                    <ul>
                        <li>
                            <strong>---</strong>
                            <span>impressions</span>
                        </li>
                        <li>
                            <strong>---</strong>
                            <span>clicks</span>
                        </li>
                        <li>
                            <strong>---</strong>
                            <span>likes</span>
                        </li>
                        <li>
                            <strong>---</strong>
                            <span>N/A</span>
                        </li>
                    </ul>
                </div>
            </div><!-- ./col -->
            <div class="col-md-3">
                <div class="social-box pinterest">
                    <i class="fa fa-pinterest"></i>
                    <ul>
                        <li>
                            <strong>---</strong>
                            <span>impressions</span>
                        </li>
                        <li>
                            <strong>---</strong>
                            <span>clicks</span>
                        </li>
                        <li>
                            <strong>---</strong>
                            <span>likes</span>
                        </li>
                        <li>
                            <strong>---</strong>
                            <span>repins</span>
                        </li>
                    </ul>
                </div>
            </div><!-- ./col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</aside><!-- /.right-side -->
</div><!-- ./wrapper -->
