<div class="product-container">
    <div class="image-frame-left">
        <img src="<?php echo base_url()."/uploads/products/".$product["app_image"]; ?>"/>
    </div>
    <div class="product-info-right">
        <div class="product-name"><?php echo $campaign_name; ?></div>

        <div class="product-description"><b>Product name:</b> <?php echo $product["name"]; ?></div>
        <div class="product-description"><b>Product Description:</b> <?php echo $product["description"]; ?></div>
        <div class="product-description"><b>Campaign Budget:</b> $<?php echo $budget; ?></div>
        <div class="product-description"><b>Campaign Timeline:</b> <?php echo $time_line; ?></div>
        <div class="product-description"><b>Key words:</b>
            <?php
            $keyword_array = explode(",",$keywords);
            foreach($keyword_array as $keyword)
            {
                $keyword = str_replace(" ","",$keyword);
                echo "#".$keyword." ";
            }
            ?>
        </div>
    </div>
</div>