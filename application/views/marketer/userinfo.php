<?php $this->load->view("includes/marketer_nav",$header_data); ?>
<div class="wrapper row-offcanvas row-offcanvas-left">

    <aside class="right-side home-right">
        <!-- Content Header (Page header) -->
        <div class="page page-dashboard" data-ng-controller="DashboardCtrl">

            <!-- Info box content -->
            <section class="content">
                <div class="callout callout-info">
                    <p>You can view and edit your personal & billing information below. <a href="#">Click here </a>if you have any questions. </p>
                </div>
            </section>
            <!-- End info content -->

            <!-- User Info Table -->
            <section class="content">

                <div class="panel panel-default">
                    <div class="panel-heading"><strong><span class="glyphicon glyphicon-user"></span> User Info</strong></div>
                    <div class="panel-body">
                    <div class="row"><br>    
                        
                    <!-- End Text Fields-->  
                        <div class="box box-primary">
                                <!-- form start -->
                            <form class="form-horizontal" role="form" method="POST" action="userinfo.php"><input type=hidden name=todo value=userinfo>
                                <div class="form-group">
                                    <label for="text" class="col-sm-2 control-label">Name</label>
                                    <div class="col-sm-7">
                                    <input type="text" class="form-control" name="name" id="name" value="<?php echo $header_data["fullname"]; ?>">
                                    </div>
                                </div>
                                                    
                                <div class="form-group">
                                    <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
                                    <div class="col-sm-7">
                                    <input type="email" class="form-control" name="email" id="email" value="<?php echo $header_data["email"]; ?>" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">Password</label>
                                    <div class="col-sm-7">
                                    <input type="password" class="form-control" name="password" id="password" value="">
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label for="text" class="col-sm-2 control-label">Interests</label>
                                    <div class="col-sm-7">
                                    <input type="text" class="form-control" name="interests" id="interests" placeholder="Use commas to separate. Ex: sports, music, art etc">
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label for="text" class="col-sm-2 control-label">Select Country</label>
                                    <div class="col-sm-5">
                                        <select id="country" name="country" class="form-control"></select>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label for="text" class="col-sm-2 control-label">State</label>
                                    <div class="col-sm-4">
                                        <select name="state" id="state" class="form-control"></select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="text" class="col-sm-2 control-label">City</label>
                                    <div class="col-sm-4">
                                    <input type="text" class="form-control" name="city" id="city" value="">
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label for="text" class="col-sm-2 control-label">Followers</label>
                                    <div class="col-sm-7">
                                    <input type="text" class="form-control" name="followers" id="followers" placeholder="Use commas to separate. Ex: sports, music, art etc">
                                    </div>
                                </div>
                                <br />
                                
                                <br /><br /><br />
                                <div class="form-group col-sm-6">
                                    <div class="col-sm-5">
                                    <p><a href="<?php echo site_url("marketer/settings"); ?>" type="button" class="btn btn-primary btn-block">Skip</a></p>
                                </div>
                                <div class="form-inline">
                                    <div class="col-sm-5">
                                    <p><button type="submit" class="btn btn-primary btn-block">Save</button></p>
                                </div>
                                </div>
                            </form>
                        </div>
                            <!-- End Text Fields-->               
                    </div>            
                </div>
            </div>
        </section>