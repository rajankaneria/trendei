<div class="form-box id="login-box">
    <div class="header bg-dark-blue">Creatives Login</div>

        <div class="body bg-gray">
            <div class="form-group">
                <input id="marketerEmail" type="email" class="form-control" name="email" placeholder="Enter email" required>
            </div>
            <div class="form-group">
                <input id="marketerPassword" type="password" name="password" class="form-control" placeholder="Password"/>
            </div>
            <div class="form-group">
                <input type="checkbox" name="remember_me"/> Remember me
            </div>
        </div>
        <div class="footer">
            <button type="button" class="btn bg-olive btn-block"  onclick="marketer_login();">Login</button>

            <p><a href="#">I forgot my password</a></p>

            <a href="<?php echo site_url("marketer/signup"); ?>" class="text-center">Register as a new user</a>
        </div>


    <div class="margin text-center">
        <span>Sign in using social networks</span>
        <br/>
        <a href="#"> <button class="btn bg-light-blue btn-circle"><i class="fa fa-facebook"></i></button></a>

    </div>
</div>