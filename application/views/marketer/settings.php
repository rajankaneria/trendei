<?php $this->load->view("includes/marketer_nav",$header_data); ?>
<div class="wrapper row-offcanvas row-offcanvas-left">

    <aside class="right-side home-right">
        <!-- Content Header (Page header) -->
        <div class="page page-dashboard" data-ng-controller="DashboardCtrl">

            <!-- Info box content -->
            <section class="content">
                <div class="callout callout-info">
                    <p>You can view and edit your personal & billing information below. <a href="#">Click here </a>if you have any questions. </p>
                </div>
            </section>
            <!-- End info content -->

            <!-- Settings Table -->
            <section class="content">
                <div class="panel panel-default">

                    <div class="box-body table-responsive no-padding">
                        <table class="table">
                            <tr>
                                <th>User Information ____ <a href="<?php echo site_url("marketer/userinfo"); ?>"> <i class="fa fa-pencil"></a></th>
                                <th></th>

                            </tr>
                            <tr>
                                <td>Full Name</td>
                                <td><?php echo $header_data["fullname"]; ?></td>

                            </tr>
                            <tr>
                                <td>Email Address</td>
                                <td><?php echo $header_data["email"]; ?></td>

                            </tr>
                            <!-- <tr>
                                <td>Address</td>
                                <td>Gandhinagar,Gujarat,India</td>

                            </tr> -->
                            <tr>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <th>Billing Information</th>
                                <th></th>
                            </tr>

                            <tr>
                                <th><img src="<?php echo asset_url(); ?>img/paypal.png"></th>
                                <th></th>

                            </tr>
                            <tr>
                                <td>Email Address</td>
                                <th></th>
                            </tr>
                            <tr>
                                <td>******</td>
                                <th></th>
                            </tr>
                        </table>
                    </div><!-- /.box-body -->
                </div>    <div class="panel panel-default">
                    <!-- End Table -->