<?php $this->load->view("includes/marketer_nav",$header_data); ?>
<!-- Right side column. Contains the navbar and content of the page -->
<?php //var_dump($main_content["app_list"]); ?>
<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="row"><br />

            <div class="headline text-center .headliner">

                <!--<p><a href="<?php echo site_url("marketer/campaigncreate"); ?>" type="button" class="btn btn-primary btn-lg btn newcampaign">Launch New Campaign</a> </p>-->
                <p><a href="#" class="btn btn-danger pull-right top-buffer" onclick="$('#newAppModal').modal('show');"><i class="glyphicon glyphicon-plus" ></i> Add New Product</a></p>
            </div><br /><br />
        </div>

        <div class="panel-heading content"><strong><span class="fa fa-send"></span> My Apps</strong></div>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="page">

            <section class="panel panel-default">
                <!--<div class="panel-heading"><strong><span class="glyphicon glyphicon-th"></span> My Apps</strong></div>-->
                <div class="panel-body">
                    <div class="row col-md-12">
                        <table class="table table table-hover table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Product Name</th>
                                <th>Product Link</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach($main_content["app_list"] as $app_row){ ?>
                                <tr id="product-<?php echo $app_row["id"]; ?>">
                                    <td class="product-name-value"><?php echo $app_row["name"]; ?></td>
                                    <td><?php if($app_row["product_link"]!=""){ ?><a id="product-link-value" href="<?php echo $app_row["product_link"]; ?>" target="_blank"><?php echo $app_row["product_link"]; ?></a><?php }else{ ?>NA<?php } ?></td>
                                    <td width=250>
                                        <a class="fa fa-eye" style="margin-right: 2em" href="#" onclick="view_app('<?php echo $app_row["id"]; ?>')"></a>
                                        &nbsp;
                                        <a class="fa fa-edit" style="margin-right: 2em" href="#" onclick="edit_product('<?php echo $app_row["id"]; ?>')"></a>
                                        &nbsp;
                                        <a class="fa fa-remove" href="#" onclick="delete_product('<?php echo $app_row["id"]; ?>');"></a>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>

        </div>
    </section><!-- /.content -->
</aside><!-- /.right-side -->
</div><!-- ./wrapper -->

<!-- COMPOSE MESSAGE MODAL -->
<div class="modal fade" id="compose-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><i class="fa fa-envelope-o"></i> Compose New Message</h4>This is id: <span class="user-id">
            </div>
            <form action="#" method="post">
                <div class="modal-body">
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon">TO:</span>
                            <input name="email_to" type="email" class="form-control" placeholder="Email TO" value="<?php echo !empty($row['appname'])?$row['id']:'';?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon">Work1:</span>
                            <input name="bookId" type="text" class="form-control" value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon">BCC:</span>
                            <input name="email_to" type="email" class="form-control" placeholder="Email BCC">
                        </div>
                    </div>
                    <div class="form-group">
                        <textarea name="message" id="email_message" class="form-control" placeholder="Message" style="height: 120px;"></textarea>
                    </div>
                    <div class="form-group">
                        <div class="btn btn-success btn-file">
                            <i class="fa fa-paperclip"></i> Attachment
                            <input type="file" name="attachment"/>
                        </div>
                        <p class="help-block">Max. 32MB</p>
                    </div>

                </div>
                <div class="modal-footer clearfix">

                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Discard</button>

                    <button type="submit" class="btn btn-primary pull-left"><i class="fa fa-envelope"></i> Send Message</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script type="text/javascript">
    $(document).ready(function() {
        $('a[data-confirm]').click(function(ev) {
            var href = $(this).attr('href');
            if (!$('#dataConfirmModal').length) {
                $('body').append('<div id="dataConfirmModal" class="modal" role="dialog" aria-labelledby="dataConfirmLabel" aria-hidden="true"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button><h3 id="dataConfirmLabel">Please Confirm</h3></div><div class="modal-body"></div><div class="modal-footer"><button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button><a class="btn btn-primary" id="dataConfirmOK">OK</a></div></div>');
            }
            $('#dataConfirmModal').find('.modal-body').text($(this).attr('data-confirm'));
            $('#dataConfirmOK').attr('href', href);
            $('#dataConfirmModal').modal({show:true});
            return false;
        });
    });

    $(function(){
        $("#newAppImageBtn").on("click",function(){ $("#newAppImageInput").click(); });
        $('#newAppImageInput').on('change', function(){
            //$("#newPortfolioImageLoader").fadeIn(300);
            var formData = new FormData($('#newAppImageForm')[0]);
            $.ajax({
                url: site_url+'/marketer/upload_app_image',
                type: 'POST',
                data: formData,
                contentType: false,
                processData: false,
                success: function(data){
                    $("#newAppImageBtn").attr("src",base_url+"/uploads/products/"+data);
                    //$("#newPortfolioImageLoader").fadeOut(300);
                }
            });
        });
    });

</script>