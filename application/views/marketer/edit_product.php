<div class="product-container">
    <div class="image-frame-left">
        <img id="editAppImageBtn" src="<?php echo base_url()."/uploads/products/".$app_image; ?>" style="cursor: pointer;"/>
    </div>
    <div class="product-info-right">
        <div class="product-name"><input type="text" id="edit_product_name" value="<?php echo $name; ?>" name="product_name" class="form-control" placeholder="Product Name"/></div>
        <div class="product-description"><textarea maxlength="105" style="margin-top: 10px;" id="edit_product_description" class="form-control" placeholder="Product Description"><?php echo $description; ?></textarea></div>
        <div class="product-link"><input type="text" id="edit_product_link" value="<?php echo $product_link; ?>" name="product_link" class="form-control" placeholder="Product Link"/></div>
    </div>
</div>
<input type="hidden" id="product_id" value="<?php echo $id ?>" />
<form id="editAppImageForm" method="post" enctype="multipart/form-data"  action="<?php echo site_url('marketer/update_app_image'); ?>" encoding="multipart/form-data">
    <input type="file" name="editAppImage" id="editAppImageInput" accept="image/*" style="display:none;">
</form>