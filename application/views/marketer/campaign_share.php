
<!DOCTYPE html>
<html itemscope itemtype="http://schema.org/Article">
    <head>

        <!-- Place this data between the <head> tags of your website -->
        <title><?php echo $product["name"]; ?></title>
        <meta name="description" content="<?php echo $product["description"]; ?>" />

        <!-- Schema.org markup for Google+ -->
        <meta itemprop="name" content="<?php echo $product["name"]; ?>">
        <meta itemprop="description" content="<?php echo $product["description"]; ?>">
        <meta itemprop="image" content="<?php echo $product["product_link"]; ?>">

        <!-- Twitter Card data -->
        <meta name="twitter:card" content="summary_large_image">
        <meta name="twitter:site" content="@publisher_handle">
        <meta name="twitter:title" content="Page Title">
        <meta name="twitter:description" content="Page description less than 200 characters">
        <meta name="twitter:creator" content="@author_handle">
        <!-- Twitter summary card with large image must be at least 280x150px -->
        <meta name="twitter:image:src" content="http://placehold.it/300x300/&text=Click%20to%20add%20photo">

        <!-- Open Graph data -->
        <meta property="og:title" content="<?php echo $product["name"]; ?>" />
        <meta property="og:type" content="article" />
        <meta property="og:url" content="<?php echo $product["product_link"]; ?>" />
        <meta property="og:image" content="http://placehold.it/300x300/&text=Click%20to%20add%20photo" />
        <meta property="og:description" content="<?php echo $product["description"]; ?>" />
        <meta property="og:site_name" content="<?php echo $product["name"]; ?>" />
        <meta property="article:tag" content="<?php echo $keywords; ?>" />
        <script>
            window.onload = function() {
                window.location.href="<?php echo $product["product_link"]; ?>";
            };
        </script>
    </head>

    <body>

    </body>
</html>
