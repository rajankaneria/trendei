<?php $this->load->view("includes/marketer_nav",$header_data); ?>
<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="row"><br />
            <div class="headline text-center .headliner">
                <p><a href="<?php echo site_url("marketer/campaigncreate"); ?>" type="button" class="btn btn-primary btn-lg btn newcampaign">Launch New Campaign</a> </p>
            </div><br /><br />
        </div>

        <div class="panel-heading content"><strong><span class="fa fa-send"></span> My Campaigns</strong></div>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="page">
            <section class="panel panel-default">
                <!--  <div class="panel-heading"><strong><span class="fa fa-send"></span> My Campaigns</strong></div>-->
                <div class="panel-body">
                    <div class="row col-md-12">
                        <table class="table table table-hover table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Campaign Name</th>
                                <th>Product Name</th>
                                <th>Timeline</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php echo $campaign_list; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
        </div>
    </section><!-- /.content -->
</aside><!-- /.right-side -->
</div><!-- ./wrapper -->