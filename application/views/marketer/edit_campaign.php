<input type="text" class="form-control" id="campaignName" value="<?php echo $campaign_name; ?>" name="campaignname" placeholder="Company Name" required/>

<input type="text" name="reservation" class="form-control pull-right" id="reservation" value="<?php echo $time_line; ?>" required placeholder="Launch Date"/>

<select class="form-control" id="campaignapp" name="campaignapp">
    <option value="">Choose an app</option>
    <?php foreach($app_list as $app_row){ ?>
        <option value='<?php echo $app_row["id"]; ?>' <?php if($product["id"]==$app_row["id"]){ echo "selected='selected'"; } ?>><?php echo $app_row["name"]; ?></option>
    <?php } ?>
</select>

<input type="text" class="form-control" id="campaignbudget" name="campaignbudget" required placeholder="Campaign Budget"  value="<?php echo $budget; ?>">

<input type="text" class="form-control" id="keywords" name="keywords" placeholder="Use comma (,) to separate multiple entries" required  value="<?php echo $keywords; ?>"/>


<div class="form-group" style="margin: 25px;">
    <div class="row">
        <?php
            $age
        ?>
        <div class="col-md-4">
            <label class="control-label">Age Group</label>
            <div class="checkbox">
                <label><input type="checkbox"  name="agegroup" value="13-17" <?php if (in_array("13-17", $age_group)){ echo "checked='checked'"; } ?>>13-17</label>
            </div>
            <div class="checkbox">
                <label><input type="checkbox"  name="agegroup" value="18-29" <?php if (in_array("18-29", $age_group)){ echo "checked='checked'"; } ?>>18-29</label>
            </div>
            <div class="checkbox">
                <label><input type="checkbox"  name="agegroup" value="30-55" <?php if (in_array("30-55", $age_group)){ echo "checked='checked'"; } ?>>30-55</label>
            </div>
            <div class="checkbox">
                <label><input type="checkbox"  name="agegroup" value="55+" <?php if (in_array("55+", $age_group)){ echo "checked='checked'"; } ?>>55+</label>
            </div>
        </div>

        <div class="col-md-4">
            <label class="control-label">Gender</label>
            <div class="radio">

                <label><input type="radio" name="gender" id="gender" value="male" <?php if($gender=="male"){ echo "checked='checked'"; } ?> > Male </label>
            </div>
            <div class="radio">
                <label><input type="radio" name="gender" id="gender" value="female" <?php if($gender=="female"){ echo "checked='checked'"; } ?>> Female </label>
            </div>
            <div class="radio">
                <label><input type="radio" name="gender" id="gender" value="both" <?php if($gender=="both"){ echo "checked='checked'"; } ?>> Both </label>
            </div>
        </div>

        <div class="col-md-4 selectContainer">

            <div class="row">
            <label class="control-label">Industry</label>
            <select class="form-control" id="industry" name="industry">
                <option value="">Select App Industry</option>
                <option value="Software" <?php if($industry_type=="Software"){ echo "selected='selected'"; } ?>>Software</option>
                <option value="Business" <?php if($industry_type=="Business"){ echo "selected='selected'"; } ?>>Business</option>
                <option value="Sport" <?php if($industry_type=="Sport"){ echo "selected='selected'"; } ?>>Sport</option>
                <option value="Others" <?php if($industry_type=="Others"){ echo "selected='selected'"; } ?>>Others</option>
            </select>
            </div>
            <div class="row" style="margin-top: 30px;">
                <label class="control-label">Preferred Network</label>
                <dl class="dl-horizontal">
                    <dd style="margin-left:0px;">
                        <label class="ui-checkbox"><input <?php if (in_array("twitter", $preferred_platform)){ echo "checked='checked'"; } ?>  name="preferredsmedia" type="checkbox" value="twitter" ><span><a href="javascript:;" class="btn-icon-round btn-icon-round-sm bg-twitter"><i class="fa fa-twitter"></i></a></span></label>
                        <label class="ui-checkbox"><input <?php if (in_array("facebook", $preferred_platform)){ echo "checked='checked'"; } ?>  name="preferredsmedia" type="checkbox" value="facebook" ><span><a href="javascript:;" class="btn-icon-round btn-icon-round-sm bg-facebook"><i class="fa fa-facebook"></i></a></span></label>
                        <label class="ui-checkbox"><input <?php if (in_array("googleplus", $preferred_platform)){ echo "checked='checked'"; } ?>  name="preferredsmedia" type="checkbox" value="googleplus"><span><a href="javascript:;" class="btn-icon-round btn-icon-round-sm bg-google-plus"><i class="fa fa-google-plus"></i></a></span></label>
                    </dd>
                </dl>
            </div>

        </div>
    </div>
</div>
<input type="hidden" value="<?php echo $id ?>" id="campaign_id" />
