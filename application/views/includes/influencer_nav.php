<header class="header">
    <a href="<?php echo site_url("influencer/dashboard"); ?>" class="logo"> <!--<img src="img/logo.jpg">--><i class="fa fa-paper-plane fa-lg"> Trendei</i>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->

        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        </a>
        <div class="navbar-right">
            <ul class="nav navbar-nav">
                <!-- <li class="dropdown user user-menu">
                    <a href="<?php echo site_url("dashboard/user_switch"); ?>">

                        <span></i>View as Marketer</span>
                    </a>
                </li> -->
                <!-- User Account: style can be found in dropdown.less -->
                <!-- <li class="dropdown user user-menu">
                    <a href="<?php echo site_url("influencer/suggestedapps"); ?>">
                        </i>
                        <span><i class="fa fa-heart"></i> New Ads</span>
                    </a>
                </li> -->
                <li class="dropdown user user-menu">
                    <a href="<?php echo site_url("influencer/dashboard"); ?>">
                        </i>
                        <span><i class="fa fa-search"></i> Browse</span>
                    </a>
                </li>
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="<?php echo site_url("influencer/campaigns"); ?>">
                        </i>
                        <span><i class="fa fa-bar-chart"></i> Shared</span>
                    </a>
                </li>
                <!-- User Account: style can be found in dropdown.less -->
                <!-- <li class="dropdown user user-menu">
                    <a href="<?php echo site_url("influencer/settings"); ?>">
                        <span><?php echo $fullname; ?></span>
                    </a>
                </li> -->
                <!-- User Account: style can be found in dropdown.less -->

            </ul>
            <ul class="nav navbar-nav">
                <!-- Messages: style can be found in dropdown.less-->

                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="glyphicon glyphicon-user"></i>

                        <span><?php echo $fullname; ?> <i class="caret"></i></span>
                    </a>

                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header bg-dark-orange">
                            <p>
                                <a href="<?php echo site_url("dashboard/user_switch"); ?>">

                                    <span></i>View as Marketer</span>
                                </a>
                            </p>
                        </li>
                        <!-- Menu Body -->
                        <!-- <li class="user-body">
                            <div class="col-xs-12 text-center">
                                <a href="<?php echo site_url("dashboard/user_switch"); ?>">

                                    <span></i>View as Marketer</span>
                                </a>
                            </div>
                        </li> -->
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="<?php echo site_url("marketer/settings"); ?>" class="btn btn-default btn-flat">Settings</a>
                            </div>
                            <div class="pull-right">
                                <a href="<?php echo site_url("marketer/logout"); ?>" class="btn btn-default btn-flat">Sign out</a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>