<!--Footer Section -->
    <input type="hidden" id="siteURL" value="<?php echo site_url(); ?>" />
    <input type="hidden" id="baseURL" value="<?php echo base_url(); ?>" />
<div class="text-center footersection"  id="footer" >
    <a href="http://twitter.com/appstyr"><i class="fa fa-twitter fa-2x color-twitter"></i></a>
    <a href="http://instagram.com/appstyr"><i class="fa fa-instagram fa-2x color-instagram"></i></a>
    <a href="http://facebook.com/appstyr"><i class="fa fa-facebook fa-2x color-facebook"></i></a>

    <br /><br />
    2014 Appstyr Inc. All Right Reserved  |
    <a href="#">Press Kit</a> |
    <a href="#">Careers</a> |
    <a href="#">Terms of Service</a>

</div>



<!-- jQuery UI 1.10.3 -->
<script src="<?php echo asset_url(); ?>js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
<!-- Bootstrap -->
<script src="<?php echo asset_url(); ?>js/bootstrap.min.js" type="text/javascript"></script>
<!-- Morris.js charts -->
<script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="<?php echo asset_url(); ?>js/plugins/morris/morris.min.js" type="text/javascript"></script>
<!-- Sparkline -->
<script src="<?php echo asset_url(); ?>js/plugins/sparkline/jquery.sparkline.min.js" type="text/javascript"></script>
<!-- jvectormap -->
<script src="<?php echo asset_url(); ?>js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js" type="text/javascript"></script>
<script src="<?php echo asset_url(); ?>js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js" type="text/javascript"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo asset_url(); ?>js/plugins/jqueryKnob/jquery.knob.js" type="text/javascript"></script>
<!-- daterangepicker -->
<script src="<?php echo asset_url(); ?>js/plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
<!-- datepicker -->
<script src="<?php echo asset_url(); ?>js/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo asset_url(); ?>js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>
<!-- iCheck -->
<script src="<?php echo asset_url(); ?>js/plugins/iCheck/icheck.min.js" type="text/javascript"></script>

<!-- AdminLTE App -->
<script src="<?php echo asset_url(); ?>js/AdminLTE/app.js" type="text/javascript"></script>

<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo asset_url(); ?>js/AdminLTE/dashboard.js" type="text/javascript"></script>

<!-- AdminLTE for demo purposes -->
<script src="<?php echo asset_url(); ?>js/AdminLTE/demo.js" type="text/javascript"></script>

<script src="<?php echo asset_url(); ?>js/script.js"></script>

<!-- page script -->
<script type="text/javascript">
    $(function() {
        $("#example1").dataTable();
        $('#example2').dataTable({
            "bPaginate": true,
            "bLengthChange": false,
            "bFilter": false,
            "bSort": true,
            "bInfo": true,
            "bAutoWidth": false
        });
    });
</script>

<script type="text/javascript">
    $(function() {
        //Datemask dd/mm/yyyy
        $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
        //Datemask2 mm/dd/yyyy
        $("#datemask2").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
        //Money Euro
        $("[data-mask]").inputmask();

        //Date range picker
        $('#reservation').daterangepicker();
        //Date range picker with time picker
        $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
        //Date range as a button
        $('#daterange-btn').daterangepicker(
            {
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
                    'Last 7 Days': [moment().subtract('days', 6), moment()],
                    'Last 30 Days': [moment().subtract('days', 29), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
                },
                startDate: moment().subtract('days', 29),
                endDate: moment()
            },
            function(start, end) {
                $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            }
        );

        //iCheck for checkbox and radio inputs
        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
            checkboxClass: 'icheckbox_minimal',
            radioClass: 'iradio_minimal'
        });
        //Red color scheme for iCheck
        $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
            checkboxClass: 'icheckbox_minimal-red',
            radioClass: 'iradio_minimal-red'
        });
        //Flat red color scheme for iCheck
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
            checkboxClass: 'icheckbox_flat-red',
            radioClass: 'iradio_flat-red'
        });

        //Colorpicker
        $(".my-colorpicker1").colorpicker();
        //color picker with addon
        $(".my-colorpicker2").colorpicker();

        //Timepicker
        $(".timepicker").timepicker({
            showInputs: false
        });
    });
</script>

<!-- view campaign popup -->
<div id="viewCampaignModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Campaign Preview</h4>
            </div>
            <div class="modal-body" style="overflow:hidden;">

            </div>
            <div class="modal-footer" style="margin-top: 0px;">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- view campaign popup ends -->


<!-- edit campaign popup -->
<div id="editCampaignModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Campaign Edit</h4>
            </div>
            <div class="modal-body" style="overflow:hidden;">

            </div>
            <div class="modal-footer" style="margin-top: 0px;">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button id="save_campaign_btn" type="button" class="btn btn-default" onclick="save_campaign();">Save</button>
            </div>
        </div>
    </div>
</div>
<!-- edit campaign popup ends -->


<!-- edit app popup -->
<div id="editAppModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Product edit</h4>
            </div>
            <div class="modal-body" style="overflow:hidden;">

            </div>
            <div class="modal-footer" style="margin-top: 0px;">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button id="save_product_btn" type="button" class="btn btn-default" onclick="save_product();">Save</button>
            </div>
        </div>
    </div>
</div>
<!-- edit app popup ends -->


<!-- view app popup -->
<div id="viewAppModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Product Preview</h4>
            </div>
            <div class="modal-body" style="overflow:hidden;">

            </div>
            <div class="modal-footer" style="margin-top: 0px;">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- view app popup ends -->




<!-- new app popup -->
<div id="newAppModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Add Product</h4>
            </div>
            <div class="modal-body" style="overflow:hidden;">
                <div style="float: left;width: 450px;">
                    <input type="text" class="form-control" id="appName" name="appName" placeholder="Product name" />
                    <textarea maxlength="105" type="text" class="form-control" id="appDescription" name="appDescription" placeholder="Product description"></textarea>
                    <input type="text" class="form-control" id="appLink" name="appLink" placeholder="Product link" />
                </div>
                <form id="newAppImageForm" method="post" enctype="multipart/form-data"  action="<?php echo site_url('marketer/upload_app_image'); ?>" encoding="multipart/form-data">
                    <input type="file" name="newAppImage" id="newAppImageInput" accept="image/*" style="display:none;">
                </form>
                <div style="float:right;">
                    <img id="newAppImageBtn" style="width: 148px; height: 132px; cursor: pointer;" src="http://placehold.it/148x132/&text=Click%20to%20add%20photo" />
                </div>
            </div>
            <div class="modal-footer" style="margin-top: 0px;">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button id="addAppBtn" type="button" class="btn btn-primary" onclick="save_application();">Add Product</button>
            </div>
        </div>
    </div>
</div>
<!-- new app popup ends -->
</body>
</html>