<?php
	$data["body_class"] = $body_class;
	$data["page_title"] = $page_title;
	$data["fullname"] = $header_data["fullname"];

	$this->load->view("includes/influencer_header.php",$data);
?>

<?php 
	$view_data["main_content"] = $main_content;
	$this->load->view($view_name,$view_data);
?>

<?php $this->load->view("includes/influencer_footer.php"); ?>