
<!DOCTYPE html>
    <html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo $page_title; ?></title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- bootstrap 3.0.2 -->
        <link href="<?php echo asset_url(); ?>css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="<?php echo asset_url(); ?>css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo asset_url(); ?>css/add-ons.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="<?php echo asset_url(); ?>css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- Morris chart -->
        <link href="<?php echo asset_url(); ?>css/morris/morris.css" rel="stylesheet" type="text/css" />
        <!-- jvectormap -->
        <link href="<?php echo asset_url(); ?>css/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
        <!-- Date Picker -->
        <link href="<?php echo asset_url(); ?>css/datepicker/datepicker3.css" rel="stylesheet" type="text/css" />
        <!-- Daterange picker -->
        <link href="<?php echo asset_url(); ?>css/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
        <!-- bootstrap wysihtml5 - text editor -->
        <link href="<?php echo asset_url(); ?>css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="<?php echo asset_url(); ?>css/AdminLTE.css" rel="stylesheet" type="text/css" />
        <!-- jQuery 2.0.2 -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <style>
            /*added by rajan */

            .image-frame-left {
                width: 40%;  float: left;
            }

            .image-frame-left img {
                width: 100%;
                height: auto;
            }

            .product-info-right {
                float: right;
                width: 60%;
                padding: 10px;
            }

            .product-name {
                font-size: 30px;
                color: #4A4A4A;
            }

            .product-description {
                font-size: 15px;
                color: #797979;
                margin-bottom: 8px;
            }

            .product-link {
                color: #A3A3A3;
                margin-top: 10px;
                text-align: center;
            }

            .product-link:hover {
                color: #0C9622;
            }

        </style>
    </head>
    <body class="<?php echo $body_class; ?>">