<!-- header logo: style can be found in header.less -->
<header class="header">
    <a href="<?php echo site_url("marketer/dashboard"); ?>" class="logo">
        <!-- Add the class icon to your logo image or logo icon to add the margining -->
        <i class="fa fa-paper-plane fa-lg"> Trendei</i>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <!--<a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>-->
        <div class="navbar-right">


            <ul class="nav navbar-nav">
                <li class="dropdown user user-menu">
                    <a href="<?php echo site_url("dashboard/user_switch"); ?>">

                        <span></i>View as Influencer</span>
                    </a>
                </li>
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="<?php echo site_url("marketer/dashboard"); ?>">

                        <span><i class="fa fa-dashboard"></i> Dashboard</span>
                    </a>
                </li>
                <li class="dropdown user user-menu">
                    <a href="<?php echo site_url("marketer/appmanagement"); ?>">

                        <span><i class="fa fa-mobile fa-lg"></i> My Products</span>
                    </a>
                </li>
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="<?php echo site_url("marketer/campaignmanagement"); ?>">

                        <span><i class="fa fa-bar-chart"></i> Campaigns</span>
                    </a>
                </li>
                <!-- User Account: style can be found in dropdown.less -->

            </ul>


            <ul class="nav navbar-nav">
                <!-- Messages: style can be found in dropdown.less-->

                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="glyphicon glyphicon-user"></i>

                        <span><?php echo $fullname; ?> <i class="caret"></i></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header bg-dark-blue">
                            <p>
                                <?php echo $fullname; ?>
                                <small>Member since 18 Nov, 2014</small>
                            </p>
                        </li>
                        <!-- Menu Body -->
                        <li class="user-body">
                            <div class="col-xs-12 text-center">
                                <a href="#">Billing Info</a>
                            </div>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="<?php echo site_url("marketer/settings"); ?>" class="btn btn-default btn-flat">Settings</a>
                            </div>
                            <div class="pull-right">
                                <a href="<?php echo site_url("marketer/logout"); ?>" class="btn btn-default btn-flat">Sign out</a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>