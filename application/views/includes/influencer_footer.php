    <input type="hidden" id="siteURL" value="<?php echo site_url(); ?>" />
    <input type="hidden" id="baseURL" value="<?php echo base_url(); ?>" />
    <!-- End App Photos -->
    <div class="text-center footersection"  id="footer" >
        <a href="http://twitter.com/appstyr"><i class="fa fa-twitter fa-2x color-twitter"></i></a>
        <a href="http://instagram.com/appstyr"><i class="fa fa-instagram fa-2x color-instagram"></i></a>
        <a href="http://facebook.com/appstyr"><i class="fa fa-facebook fa-2x color-facebook"></i></a>

        <br /><br />
        2014 Appstyr Inc. All Right Reserved |
        <a href="#"> Press Kit</a> |
        <a href="#">Careers</a> |
        <a href="#">Terms of Service</a>
        
        
    </div>


    <!-- jQuery 2.0.2 -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
    <script src="<?php echo asset_url(); ?>js/script.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo asset_url(); ?>js/bootstrap.min.js" type="text/javascript"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo asset_url(); ?>js/AdminLTE/app.js" type="text/javascript"></script>


    </body>
</html>