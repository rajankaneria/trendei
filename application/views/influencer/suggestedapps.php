<?php $this->load->view("includes/influencer_nav"); ?>
<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side home-right">
    <!-- Content Header (Page header) -->
    <div class="page page-dashboard" data-ng-controller="DashboardCtrl">

        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row"><br />
                <div class="headline text-center .headliner">
                    Suggested Ads
                    <h4 class="subheader">Recommendations based on your interest</h4>
                </div><br /><br />
            </div><!-- /.row -->
            <br /><br />
            <div class="panel panel-default">

                <!-- <div class="panel-heading"><strong><span class="glyphicon glyphicon-th"></span> Recommended Campaigns</strong></div>-->
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-4">
                            <a href="#" class="text-center hashtags">#sports</a>
                            <a href="#" class="text-center hashtags">#music</a>
                            <a href="#" class="text-center hashtags">#tech</a>
                            <div class="thumbnail">
                                <img alt="" src="<?php echo asset_url(); ?>img/apppic3.png">
                                <div class="caption">
                                    <h3>Appstyr</h3>
                                    <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
                                    <p>Launch Date</a></p>
                                    <p><a href="quemsg.html" type="button" class="btn btn-primary btn-lg btn-block">Share</a></p>
                                </div>

                            </div>
                        </div>

                        <div class="col-sm-4">
                            <a href="#" class="text-center hashtags">#sports</a>
                            <a href="#" class="text-center hashtags">#music</a>
                            <a href="#" class="text-center hashtags">#tech</a>
                            <div class="thumbnail">
                                <img alt="" src="<?php echo asset_url(); ?>img/apppic1.png">
                                <div class="caption text">
                                    <h3>Appstyr</h3>
                                    <p>Appstyr helps users find new and relevant mobile apps with the help of friends. Appstyr lets people easily communicate socially and share apps the way they do music, photos etc.</p>
                                    <p><a href="#" class="text-center">Launch Date</a></p>
                                    <p><a href="quemsg.html" type="button" class="btn btn-primary btn-lg btn-block">Share</a> </p>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <a href="#" class="text-center hashtags">#sports</a>
                            <a href="#" class="text-center hashtags">#music</a>
                            <a href="#" class="text-center hashtags">#tech</a>
                            <div class="thumbnail">
                                <img alt="" src="<?php echo asset_url(); ?>img/apppic4.png">
                                <div class="caption text">
                                    <h3>App Name</h3>
                                    <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
                                    <p><a href="#" class="text-center">Launch Date</a></p>
                                    <p><a href="quemsg.html" type="button" class="btn btn-primary btn-lg btn-block">Share</a> </p>
                                </div>
                            </div><br /><br />
                        </div>
                        <!-- Top row of three -->
                    </div>
                </div>
            </div>