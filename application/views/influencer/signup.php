<div class="form-box" id="login-box">
    <div class="header">Signup as Influencer</div>

        <div class="body bg-white">
            <a class="btn btn-block btn-social btn-facebook">
                <i class="fa fa-facebook"></i> Connect with Facebook
            </a>
            <div class="form-group">
                <input id="influencerName" type="text" name="name" class="form-control" placeholder="Full name"/>
            </div>
            <div class="form-group">
                <input type="text" id="influencerEmail" class="form-control" name="influencerInputEmail1" placeholder="Enter email" required>
            </div>
            <div class="form-group">
                <input type="password" id="influencerPassword" name="password" class="form-control" placeholder="Password"/>
            </div>
            <div class="form-group">
                <input type="password" id="influencerPassword2" name="password2" class="form-control" placeholder="Retype password"/>
            </div>
            <div class="form-group">
                I agree to terms and conditions  <input id="influencerAgreement" type=checkbox name=agree value='yes'/>
            </div>
        </div>

        <div class="footer">

            <button type="button" class="btn bg-olive btn-block" onclick="influencer_signup();">Sign me up</button>

            Already signed up? <a href="<?php echo site_url("influencer/login"); ?>" class="text-center"> Login</a>
        </div>

    <div class="margin text-center">
        <span>_ </span>
    </div>
</div>