<?php $this->load->view("includes/influencer_nav"); ?>
<!-- Right side column. Contains the navbar and content of the page -->
<div class="wrapper row-offcanvas row-offcanvas-left">
    <!-- Left side column. contains the logo and sidebar -->
    <!--<aside class="left-side sidebar-offcanvas">
        <section class="sidebar">
            <ul class="sidebar-menu">
                <li>
                    <a href="../index.html">
                        <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                    </a>
                </li>
                <li class="active">
                    <a href="#">
                        <i class="fa fa-bar-chart-o">
                        </i> <span>My campaigns</span> <small class="badge pull-right bg-red">4</small>
                    </a>
                </li>
                <li class="treeview">
                    <a href="settings.php">
                        <i class="fa fa-gear"></i> <span>Settings</span>
                    </a>
                </li>
            </ul>
        </section>
    </aside> -->
    <!-- Right side column. Contains the navbar and content of the page -->
    <aside class="right-side home-right">
        <!-- Content Header (Page header) -->
        <div class="page page-dashboard" data-ng-controller="DashboardCtrl">
            <!-- User Statistics -->
            <!-- /.row -->

            <!-- Main content -->
            <!-- Info box content -->
            <section class="content">
                <div class="callout callout-info">
                    <p>You can view and edit your personal & billing information below. <a href="#">Click here </a>if you have any questions. </p>
                </div>
            </section>


            <!-- End info content -->
            <!-- Campaigns Table -->
            <div class="panel panel-default"><br />
                <div class="panel-heading"><strong><span class="glyphicon glyphicon-th"></span> My Campaigns</strong></div>
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tr>
                            <th>#</th>
                            <th>Campaign Name</th>
                            <th>Date</th>
                            <th>Status</th>
                            <th>Status</th>
                            <th>Earnings</th>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td>Captionit</td>
                            <td>11-7-2014</td>
                            <td><span class="label label-success">Live</span></td>
                            <td>Test</td>
                            <td>Test</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>Shindy</td>
                            <td>11-7-2014</td>
                            <td><span class="label label-success">Live</span></td>
                            <td>Test</td>
                            <td>Test</td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>Uqubi</td>
                            <td>11-7-2014</td>
                            <td><span class="label label-danger">Ended</span></td>
                            <td>Test</td>
                            <td>Test</td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>OMN</td>
                            <td>11-7-2014</td>
                            <td><span class="label label-danger">Ended</span></td>
                            <td>Test</td>
                            <td>Test</td>
                        </tr>
                    </table>
                </div><!-- /.box-body -->
            </div>
            <!-- End Campaigns Table -->