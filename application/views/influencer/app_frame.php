<?php if(count($campaign_array)!=0){ ?>
<?php foreach($campaign_array as $campaign_row){ ?>
<div class="col-sm-4">
    <?php
        $keyword_array = explode(",",$campaign_row["keywords"]);
        foreach($keyword_array as $keyword)
        {
            $keyword = str_replace(" ","",$keyword);
    ?>
    <a href="#" class="text-center hashtags">#<?php echo $keyword; ?></a>
    <?php } ?>
    <div class="thumbnail">
            <?php if($campaign_row["product"]["app_image"]!=""){ ?><img alt="" src="<?php echo base_url(); ?>uploads/products/<?php echo $campaign_row["product"]["app_image"]; ?>"><?php }else{ ?>
                <img alt="" src="http://placehold.it/500x500/&text=Photo%20Not%20Available">
            <?php } ?>
            <div class="caption">
                <h3><?php echo $campaign_row["product"]["name"]; ?></h3>
                <p class="product-desc"><?php echo $campaign_row["product"]["description"]; ?></p>
                <p class="app-lunchdate">Launch Date</a></p>
                <p><a href="javascript:fbShare('http://google.com/', 'Google Title', 'This is stupid description of code', 'http://jonbennallick.co.uk/wp-content/uploads/2012/08/Facebook-logo-ICON-02.png', 520, 350)" type="button" class="btn btn-primary btn-lg btn-block">Share</a></p>
            </div>
    </div>
</div>
    <?php } ?>
<?php }else{ ?>
        <div style="text-align: center; margin:50px;">No campaign added yet!</div>
<?php } ?>