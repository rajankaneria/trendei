<?php $this->load->view("includes/influencer_nav"); ?>
<div class="wrapper row-offcanvas row-offcanvas-left">
    <!-- Left side column. contains the logo and sidebar -->
    <!--<aside class="left-side sidebar-offcanvas">
        <section class="sidebar">
            <ul class="sidebar-menu">
                <li>
                    <a href="../index.html">
                        <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                    </a>
                </li>
                <li>
                    <a href="campaigns.php">
                        <i class="fa fa-bar-chart-o">
                        </i> <span>My campaigns</span> <small class="badge pull-right bg-red">4</small>
                    </a>
                </li>
                <li class="treeview active">
                    <a href="#">
                        <i class="fa fa-gear"></i> <span>Settings</span>
                    </a>
                </li>
            </ul>
        </section>
    </aside>-->
    <aside class="right-side home-right">
        <!-- Content Header (Page header) -->
        <div class="page page-dashboard" data-ng-controller="DashboardCtrl">

            <!-- Info box content -->
            <section class="content">
                <div class="callout callout-info">
                    <p>You can view and edit your personal & billing information below. <a href="#">Click here </a>if you have any questions. </p>
                </div>
            </section>
            <!-- End info content -->

            <!-- Settings Table -->
            <section class="content">
                <div class="panel panel-default">

                    <div class="box-body table-responsive no-padding">
                        <table class="table">
                            <tr>
                                <th>User Information ____ <a href="userinfo.php"> <i class="fa fa-pencil"></a></th>
                                <th></th>

                            </tr>
                            <tr>
                                <td>Full Name</td>
                                <td>Rajan Kaneria</td>

                            </tr>
                            <tr>
                                <td>Email Address</td>
                                <td>rajan.kaneria@gmail.com</td>

                            </tr>
                            <tr>
                                <td>Address</td>
                                <td>Gandhinagar,Gujarat,India</td>

                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <th>Billing Information</th>
                                <th></th>
                            </tr>

                            <tr>
                                <th><img src="<?php echo asset_url(); ?>img/paypal.png"></th>
                                <th></th>

                            </tr>
                            <tr>
                                <td>Email Address</td>
                                <th></th>
                            </tr>
                            <tr>
                                <td>******</td>
                                <th></th>
                            </tr>
                        </table>
                        <p class="content"> <a href="login.html"> Logout </a></p>
                    </div><!-- /.box-body -->
                </div>    <div class="panel panel-default">
                    <!-- End Table -->