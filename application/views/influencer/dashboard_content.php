<script>
    function fbShare(url, title, descr, image, winWidth, winHeight) {
        var winTop = (screen.height / 2) - (winHeight / 2);
        var winLeft = (screen.width / 2) - (winWidth / 2);
        window.open('http://www.facebook.com/sharer.php?s=100&p[title]=' + title + '&p[summary]=' + descr + '&p[url]=' + url + '&p[images][0]=' + image, 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);
    }
</script>
<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side home-right">
    <!-- Content Header (Page header) -->
    <div class="page page-dashboard" data-ng-controller="DashboardCtrl">

        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row"><br />
                <div class="headline text-center .headliner">
                    Launching Soon
                    <h4 class="subheader">Great new apps coming soon</h4>
                </div><br /><br />
            </div><!-- /.row -->
            <br /><br />

            <div class="panel panel-default">

                <!-- <div class="panel-heading"><strong><span class="glyphicon glyphicon-th"></span> Recommended Campaigns</strong></div>-->
                <div class="panel-body">
                    <div class="row">
                        <?php echo $campaign_list; ?>
                    </div>
                </div>
            </div>