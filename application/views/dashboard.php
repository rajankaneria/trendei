<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
<!--<![endif]-->
<!-- HEAD SECTION -->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <title>Trendei</title>
    <!-- Favicon Image. -->
    <link rel="icon" href="assets/img/favicon.png" type="image/x-icon" />
    <!--GOOGLE FONT -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    <!--BOOTSTRAP MAIN STYLES -->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
    <!--FONTAWESOME MAIN STYLE -->
    <link href="assets/css/font-awesome.min.css" rel="stylesheet" />
    <!--CUSTOM STYLE -->
    <link href="assets/css/style.css" rel="stylesheet" />
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
    <!--END HEAD SECTION -->
<body>   
     <!-- NAV SECTION ============================================================ -->
    <div class="navbar navbar-inverse navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                </button>
                <a class="navbar-brand fa fa-send" href="#"> Trendei</a>
            </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#about-section">ABOUT</a></li>
                    <li><a href="#marketer-section">CREATORS</a></li>
                    <li><a href="#influencer-section">INFLUENCERS</a></li>
                </ul>
            </div>
        </div>
    </div>
     <!--END NAV SECTION -->
     <!-- HEADER SECTION (VIDEO HEADER) ============================================================ -->
   <section id="story-header">
 
   <div id="parallax-video" class="horizontalsection 
   text-light videobg-section">
   <div class="vid-overlay">
   </div>              
           	<div class="horizontalinner wrapper">
                  <h6 class="align-center sr-animation sr-animation-frombottom animated"></h6>
              		<p class="align-center sr-animation sr-animation-frombottom animated" data-delay="200">
                    	<div class="space-bottom"></div>
                        <h1 class="intro-text text-center">Discover new products first.</h1>
                    	<div class="space-middle"></div>
                    	<hr class="hr-short-2">
                    	<div class="lpphotos col-sm-10">		
                    	
                        <div class="col-sm-3">
                            <div class="thumbnail">
                                <img src="assets/img/apppic6.png">
                                <div class="caption text">
                                    <h4>SeeNuu</h4>
                                    <span>Showcase your accomplishments and share your goals.</span>
                                    <a href="#"><p>Share this!</p></a>
                                    <p><a href="<?php echo site_url("influencer/login"); ?>" type="button" class="btn btn-default btn-lg btn-block">Launching: 1/1/15 <i class="fa fa-send"></i></a> </p>
                                </div>
                            </div>    
                        </div>
                        
                         <div class="col-sm-3">
                            <div class="thumbnail">
                                <img src="assets/img/apppic5.png">
                                <div class="caption text">
                                    <h4>Appstyr (ios App)</h4>
                                    <span>Discover & share apps with friends the same you do music, photos, memes etc.</span>
                                    <a href="#"><p>Share this!</p></a>
                                    <p><a href="<?php echo site_url("influencer/login"); ?>" class="btn btn-default btn-lg btn-block">Launching: 1/1/15 <i class="fa fa-send"></i></a> </p>
                                </div>
                            </div>    
                        </div>
                        
                        <div class="col-sm-3">
                            <div class="thumbnail">
                                <img src="assets/img/apppic2.png">
                                <div class="caption text">
                                    <h4>Ambazi</h4>
                                    <span>Ambazi connects college students with companies that need ambassadors.</span>
                                    <a href="#"><p>Share this!</p></a>
                                    <p><a href="<?php echo site_url("influencer/login"); ?>" type="button" class="btn btn-default btn-lg btn-block">Launching: 1/1/15 <i class="fa fa-send"></i></a> </p>
                                </div>
                            </div><br /><br />    
                        </div>

                    	</div>  
                    	</br></br></br></br>
                    	<div class="col-md-12">
                		<div class="text-center">
                    	<!--<a href="http://www.get.appstyr.com">
                        <button type="submit" class="btn btn-primary"><img src="assets/img/dlbtn.jpg"></button></a>-->
                	</div>                      
            	</div>
          	</div>   
			<div class="vid-bg">
			<video id="video0" preload="auto" autoplay="autoplay" loop="loop" muted="muted" poster="assets/img/bg1.jpg" style="top: -61.3333333333333px; left: 0px; position: relative; z-index: -11; width: 1280px; height: 810px;">
			<source src="assets/vid/steven.mp4" type="video/mp4">
			<source src="assets/vid/steven.webm" type="video/webm">bgvideo</video></div></div>      
        <div class="parallax" data-velocity="-.3" ></div>
 
     
     <!-- IMAGE HEADER -->     
    		<!---->
    
    </section>
    
      <!--END HEADER SECTION -->
   
     <!-- ABOUT SECTION ============================================================ -->   
    <div id="about-section" class="section"></div>
    <div class="container"  >
        <div class="row main-low-margin text-center" >               
            <div class="text-center" >
            	<h3 class="mid-section-text" >A place for creators & influencers!</h3>
                	<div class="space-bottom"></div>
            </div>
            <div class="text-center" >
		 		<div class="col-md-6 col-sm-6" >
		 			<a href="<?php echo site_url("marketer/login"); ?>"><button type="text" class="btn btn-creator btn-block fa fa-laptop"> I'm a creator</button></a><br>
		 			<h3 class="section-sub-text "> I’m launching a new product <br> that people will love!</h3>  
       				<hr class="hr-short">
                </div>               
                <div class="col-md-6 col-sm-6" >
                	<a href="<?php echo site_url("influencer/login"); ?>"><button type="text" class="btn btn-influencer btn-block fa fa-share-alt"> I'm an influencer</button></a><br>
                	
                	<h3 class="section-sub-text"> I would love to find new products <br> to share with my followers!</h3>
                	<hr class="hr-short">
                </div>
            </div>
        </div>
    </div><div class="space-middle"></div>
        <!--END ABOUT SECTION -->
     <!--PARALLAX SECTION ONE ============================================================ -->
    <section id="story-freext">
        
    	<article>
       
        	<span>
            </span>
          
        
        </article><div class="parallax" data-velocity="-.3" ></div>

    
    </section>
    <!-- END PARALLAX SECTION ONE -->
<!-- About#2 ============================================================ -->
	<div id="marketer-section" class="container" > <div class="space-middle"></div>
        <div class="text-center" >               
            <div class="text-center" >
            	<h3 class="mid-section-text" >List your upcoming product(s) for free!</h3>
               
            </div>
            <div class="text-center" >
		 		<div>
		 			<h3 class="section-sub-text"> Whether you're launching a new app/website or you're releasing your first <br> single, Trendei can help <i>"boost"</i> your launch-day efforts.</h3>  
       				<hr class="hr-short">
       				<h1>
			<a href="<?php echo site_url("marketer/login"); ?>"><h3>List your product!</h3></a>
                </div>               
            </div>
        </div>
        
	</div> <div class="space-bottom"></div>

<!--PARALLAX SECTION TWO ============================================================ -->
	<section id="story-two">
        
    	<article>
       
        	<span>
        		   
        	</span>
          
        
    	</article><div class="parallax" data-velocity="-.3" ></div>
    
	</section>
    <!--END PARALLAX SECTION TWO -->     
<!-- About#3 ============================================================ -->	
	<div id="influencer-section" class="section"></div><div class="space-middle"></div>
		<div class="text-center" >               
            <div class="text-center" >
            	
            	<h3 class="mid-section-text" >Get rewarded just for sharing!</h3>
                
            </div>
            <div class="text-center" >
		 		<div>
		 			<h3 class="section-sub-text"> You can earn cash & other rewards by simply sharing <br> upcoming products you love with your followers. </h3> 
       				<hr class="hr-short">
       				<a href="<?php echo site_url("influencer/login"); ?>"><h3>Get Started!</h3></a>
       				
                </div>               
            </div>
        </div>
	</div>
 <div class="space-bottom"></div>

<!--PARALLAX SECTION THREE ============================================================ -->
	<div id="signup-section" class="section"></div>
	<section id="story-three">
        
    	<article>
       
        	<span>
        	
     	   </span>
          
        
    	</article><div class="parallax" data-velocity="-.3" ></div>
    
	</section>
    <!--END PARALLAX SECTION TWO --> 
<!--CONTACT SECTION ******************************************************************CONTACT SECTION**********-->	
	<!--<div id="developers-section" class="section"></div>		
		<div class="container">   
			<div class="col-sm-6">
				<div class="text-center"><div class="space-top"></div>
					<h2 class="developers"> Developers: List your app(s) for free. </h2><br />              	           
					<h3 class="devinfo">Whether you're launching a new app, or you need exposure for your existing app, <br> we can help. Sign up to learn how Appstyr can <i>"boost"</i> your launch-day efforts.</3h>          		
				
				</div>
			</div>
		</div>
		
	<div class="text-center" >
		 		<div class="col-md-3 col-sm-3" >
		 		<i class="fa fa-pencil fa-2x color-blue"></i>
                    <h3>LIST</h3>           
                </div>               
                <div class="col-md-3 col-sm-3" >
                <i class="fa fa-thumbs-up fa-2x color-black"></i>
                    <h3>CHOOSE</h3>
                </div>
                <div class="col-md-3 col-sm-3" >
                <i class="fa fa-pencil fa-2x color-blue"></i>
                    <h3>TRACK</h3>
                </div>
                <div class="col-md-3 col-sm-3" >
                <i class="fa fa-flask fa-2x color-brown"></i>
                    <h3>TRACK</h3>
                </div>
            </div>	 -->  	
         
    <!--END FOOTER SECTION --> 
    <!-- JAVASCRIPT FILES PLACED AT THE BOTTOM TO REDUCE THE LOADING TIME  -->
    <!-- CORE JQUERY LIBRARY -->
    <script src="assets/js/jquery.js"></script>
    <!-- CORE BOOTSTRAP LIBRARY -->
    <script src="assets/js/bootstrap.min.js"></script>
     <!-- PARALLAX SCRIPTS-->
     <script src="assets/js/scrolly-master/jquery.scrolly.js"></script>
       <!-- CUSTOM SCRIPT-->  
    <script src="assets/js/custom.js"></script>
    
</body>
</html>
