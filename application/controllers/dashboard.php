<?php

class dashboard extends CI_Controller {
		
	public function index()
	{
		$header_data = array(
			"fullname" => "",
			"email" => ""
		);
		$data = array(
			"page_title" => "Trendei | Dashboard",
			"view_name" => "dashboard",
			"main_content" => "",
			"header_data" => $header_data,
			"body_class" => ""
		);
		$this->load->view('includes/influencer_template',$data);
	}
	public function  user_switch()
	{
		$session_userid = $this->session->userdata('user_mem_id');
		$this->load->model("user_model");
		$redirect_controller = $this->user_model->switch_check($session_userid);
		redirect($redirect_controller);
	}
	
}

