<?php

class influencer extends CI_Controller {

	public function dashboard()
	{
		$session_email = $this->session->userdata('email');
		$session_fullname = $this->session->userdata('fullname');
		if(empty($session_email))
		{
			$main_content = $this->load->view("login_message","",TRUE);
		}
		else
		{
			$header_data = array(
				"fullname" => $session_fullname,
				"email" => $session_email
			);
			$this->load->model("campaign_model");
			$campaign_array = $this->campaign_model->get_all_campaign();
			$campaign_list = $this->load->view("influencer/app_frame",array("campaign_array" => $campaign_array),TRUE);
			$main_content = "";

			$main_content .= $this->load->view("includes/influencer_nav",$header_data,TRUE);
			$main_content .= $this->load->view("influencer/dashboard_content",array("campaign_list" => $campaign_list),TRUE);
		}
		/*
		// bypass the login check for demo purpose : start //
		$main_content = "";
		$main_content .= $this->load->view("includes/influencer_nav","",TRUE);
		$main_content .= $this->load->view("influencer/dashboard_content","",TRUE);
		// bypass the login check for demo purpose : end //
		*/
		$data = array(
			"page_title" => "Trendei | Dashboard",
			"view_name" => "influencer/dashboard",
			"main_content" => $main_content,
			"header_data" => $header_data,
			"body_class" => "skin-orange"

		);
		$this->load->view('includes/influencer_template',$data);
	}
	public function login()
	{
		$session_email = $this->session->userdata('email');
		$session_fullname = $this->session->userdata('fullname');
		$header_data = array(
			"fullname" => $session_fullname,
			"email" => $session_email
		);
		$data = array(
			"page_title" => "Trendei | Log in",
			"view_name" => "influencer/login",
			"main_content" => "",
			"header_data" => $header_data,
			"body_class" => "lockscreen_influencer"
		);
		$this->load->view('includes/influencer_template',$data);
	}
	public function signup()
	{
		$session_email = $this->session->userdata('email');
		$session_fullname = $this->session->userdata('fullname');
		$header_data = array(
			"fullname" => $session_fullname,
			"email" => $session_email
		);
		$data = array(
			"page_title" => "Trendei | Signup",
			"view_name" => "influencer/signup",
			"main_content" => "",
			"header_data" => $header_data,
			"body_class" => "lockscreen_influencer"
		);
		$this->load->view('includes/influencer_template',$data);
	}
	public function campaigns()
	{
		$session_email = $this->session->userdata('email');
		$session_fullname = $this->session->userdata('fullname');
		$header_data = array(
			"fullname" => $session_fullname,
			"email" => $session_email
		);
		$data = array(
			"page_title" => "Trendei | Campaigns",
			"view_name" => "influencer/campaigns",
			"main_content" => "",
			"header_data" => $header_data,
			"body_class" => "skin-orange"
		);
		$this->load->view('includes/influencer_template',$data);
	}
	public function settings()
	{
		$session_email = $this->session->userdata('email');
		$session_fullname = $this->session->userdata('fullname');
		$header_data = array(
			"fullname" => $session_fullname,
			"email" => $session_email
		);
		$data = array(
			"page_title" => "Trendei | Settings",
			"view_name" => "influencer/settings",
			"main_content" => "",
			"header_data" => $header_data,
			"body_class" => "skin-orange"
		);
		$this->load->view('includes/influencer_template',$data);
	}
	public function suggestedapps()
	{
		$session_email = $this->session->userdata('email');
		$session_fullname = $this->session->userdata('fullname');
		$header_data = array(
			"fullname" => $session_fullname,
			"email" => $session_email
		);
		$data = array(
			"page_title" => "Trendei | Suggested Apps",
			"view_name" => "influencer/suggestedapps",
			"main_content" => "",
			"header_data" => $header_data,
			"body_class" => "skin-orange"
		);
		$this->load->view('includes/influencer_template',$data);
	}
	public function user_signup()
	{
		$user_input = $this->input->post("userData");
		//$user_input["user_type"] = "influencer";
		$user_input["password"] = md5($user_input["password"]);
		//$user_input["email"] = $user_input["email"];
		$this->load->model("user_model");
		$output = $this->user_model->create_influencer($user_input);
		echo $output;
	}
	public function user_login()
	{
		$user_input = $this->input->post("userData");
		$user_input["password"] = md5($user_input["password"]);
		$this->load->model("user_model");
		$output = $this->user_model->login($user_input);
		echo $output;
	}
	
}

