<?php

class marketer extends CI_Controller {

	public function dashboard()
	{

        $session_user_id = $this->session->userdata('user_mem_id');
        $session_email = $this->session->userdata('email');
        $session_fullname = $this->session->userdata('fullname');
        if(empty($session_email))
        {
        	$main_content = $this->load->view("login_message","",TRUE);
        }
        else
        {
			$header_data = array(
				"fullname" => $session_fullname,
				"email" => $session_email
			);

            $main_content = "";
            $main_content .= $this->load->view("includes/marketer_nav",$header_data,TRUE);
            $main_content .= $this->load->view("marketer/dashboard_content","",TRUE);
        }
        /*
		// bypass the login check for demo purpose : start //
		$main_content = "";
		$main_content .= $this->load->view("includes/marketer_nav","",TRUE);
		$main_content .= $this->load->view("marketer/dashboard_content","",TRUE);
		// bypass the login check for demo purpose : end //
        */
		$data = array(
			"page_title" => "Trendei | Dashboard",
			"view_name" => "marketer/dashboard",
			"main_content" => $main_content,
			"body_class" => "skin-blue"
		);
		$this->load->view('includes/marketer_template',$data);
	}
	public function logout()
	{
		$this->session->sess_destroy();
		redirect('marketer/login');
	}
	public function login()
	{
		$data = array(
			"page_title" => "Trendei | Log in",
			"view_name" => "marketer/login",
			"main_content" => "",
			"body_class" => "lockscreen_marketer"
		);
		$this->load->view('includes/marketer_template',$data);
	}
	public function signup()
	{
		$data = array(
			"page_title" => "Trendei | Signup",
			"view_name" => "marketer/signup",
			"main_content" => "",
			"body_class" => "lockscreen_marketer"
		);
		$this->load->view('includes/marketer_template',$data);
	}
	public function campaignmanagement()
	{

		$user_mem_id = $this->session->userdata('user_mem_id');
		$session_fullname = $this->session->userdata('fullname');
		$session_email = $this->session->userdata('email');
		$this->load->model("campaign_model");
		$campaign_array = $this->campaign_model->get_campaign($user_mem_id);
		$campaign_list = $this->load->view("marketer/campaign_row",array("campaign_array" => $campaign_array),TRUE);

		$header_data = array(
			"fullname" => $session_fullname,
			"email" => $session_email
		);
		$data = array(
			"page_title" => "Trendei | Campaign Management",
			"view_name" => "marketer/campaignmanagement",
			"main_content" => "",
			"header_data" => $header_data,
			"body_class" => "skin-blue",
			"campaign_list" => $campaign_list
		);
		$this->load->view('includes/marketer_template',$data);
	}
	public function settings()
	{
		$user_mem_id = $this->session->userdata('user_mem_id');
		$session_fullname = $this->session->userdata('fullname');
		$session_email = $this->session->userdata('email');
		$header_data = array(
			"fullname" => $session_fullname,
			"email" => $session_email
		);
		$data = array(
			"page_title" => "Trendei | Settings",
			"view_name" => "marketer/settings",
			"main_content" => "",
			"header_data" => $header_data,
			"body_class" => "skin-blue"
		);
		$this->load->view('includes/marketer_template',$data);
	}
	public function appmanagement()
	{
		$this->load->model("product_model");
		$main_content = array(
			"app_list" => $this->product_model->get_product($this->session->userdata('user_mem_id'))
		);
		$session_email = $this->session->userdata('email');
		$session_fullname = $this->session->userdata('fullname');
		$header_data = array(
			"fullname" => $session_fullname,
			"email" => $session_email
		);
		$data = array(
			"page_title" => "Trendei | Application Management",
			"view_name" => "marketer/appmanagement",
			"main_content" => $main_content,
			"header_data" => $header_data,
			"body_class" => "skin-blue"
		);
		$this->load->view('includes/marketer_template',$data);
	}
	public function campaigncreate()
	{
		$session_email = $this->session->userdata('email');
		$session_fullname = $this->session->userdata('fullname');
		$header_data = array(
			"fullname" => $session_fullname,
			"email" => $session_email
		);

		$this->load->model("product_model");
		$main_content = array(
			"app_list" => $this->product_model->get_product($this->session->userdata('user_mem_id'))
		);

		$data = array(
			"page_title" => "Trendei | Create Campaign",
			"view_name" => "marketer/campaigncreate",
			"main_content" => $main_content,
			"header_data" => $header_data,
			"body_class" => "skin-blue"
		);
		$this->load->view('includes/marketer_template',$data);
	}

	public function user_signup()
	{
		$user_input = $this->input->post("userData");
		//$user_input["user_type"] = "influencer";
		$user_input["password"] = md5($user_input["password"]);
		//$user_input["email"] = $user_input["email"];
		$this->load->model("user_model");
		$output = $this->user_model->create_marketer($user_input);
		echo $output;
	}

	public function marketer_create_campaign()
	{
		$campaign_input = $this->input->post("campaignData");
		$this->load->model("campaign_model");
		$output = $this->campaign_model->campaign_create($campaign_input);
		echo $output;
	}
	public function user_login()
	{
		$user_input = $this->input->post("userData");
		$user_input["password"] = md5($user_input["password"]);
		$this->load->model("user_model");
		$output = $this->user_model->login($user_input);
		echo $output;
	}
	public function add_product()
	{
		$app_input = $this->input->post("applicationData");
		$this->load->model("product_model");
		$output = $this->product_model->product_create($app_input);
		echo $output;
	}
	public function upload_app_image()
	{
		$upload_data = array(
			"upload_input" => "newAppImage",
			"upload_folder" => "uploads/products/",
			"upload_type" => "product"
		);
		$this->load->model("photo_upload");
		$result = $this->photo_upload->upload_picture($upload_data);
		echo $result;

	}
	
	public function userinfo()
	{
		$user_mem_id = $this->session->userdata('user_mem_id');
		$session_fullname = $this->session->userdata('fullname');
		$session_email = $this->session->userdata('email');
		$header_data = array(
			"fullname" => $session_fullname,
			"email" => $session_email
		);
		$data = array(
			"page_title" => "Trendei | UserInfo",
			"view_name" => "marketer/userinfo",
			"main_content" => "",
			"header_data" => $header_data,
			"body_class" => "skin-blue"
		);
		$this->load->view('includes/marketer_template',$data);
	}
	public function view_product()
	{
		$product_id = $this->input->post("product_id");
		$this->load->model("product_model");
		$product_array = $this->product_model->get_row($product_id);
		$output = $this->load->view("marketer/view_product",$product_array,TRUE);
		echo $output;
	}
	public function delete_product()
	{
		$product_id = $this->input->post("product_id");
		$this->load->model("product_model");
		$this->product_model->delete_row($product_id);
	}
	public function edit_product()
	{
		$product_id = $this->input->post("product_id");
		$this->load->model("product_model");
		$product_array = $this->product_model->get_row($product_id);
		$data = array(
			"temp_product_img" => $product_array["app_image"]
		);
		$this->session->set_userdata($data);
		$output = $this->load->view("marketer/edit_product",$product_array,TRUE);
		echo $output;
	}
	public function update_product()
	{
		$product_id = $this->input->post("product_id");
		$productData = $this->input->post("productData");

		$this->load->model("product_model");
		$this->product_model->update_row($product_id,$productData);
	}
	public function update_app_image()
	{
		$upload_data = array(
			"upload_input" => "editAppImage",
			"upload_folder" => "uploads/products/",
			"upload_type" => "product"
		);
		$this->load->model("photo_upload");
		$result = $this->photo_upload->upload_picture($upload_data);
		echo $result;
	}
	public function view_campaign()
	{
		$campaign_id = $this->input->post("campaign_id");
		$this->load->model("campaign_model");
		$campaign_array = $this->campaign_model->get_row($campaign_id);
		$output = $this->load->view("marketer/view_campaign",$campaign_array,TRUE);
		echo $output;
	}
	public function edit_campaign()
	{
		$campaign_id = $this->input->post("campaign_id");
		$this->load->model("campaign_model");
		$this->load->model("product_model");
		$campaign_array = $this->campaign_model->get_row($campaign_id);

		$campaign_array["app_list"] = $this->product_model->get_product($this->session->userdata('user_mem_id'));
		$campaign_array["age_group"] = explode(",",$campaign_array["age_group"]);
		$campaign_array["preferred_platform"] = explode(",",$campaign_array["preferred_platform"]);
		$output = $this->load->view("marketer/edit_campaign",$campaign_array,TRUE);
		echo $output;
	}
	public function marketer_update_campaign()
	{
		$campaign_id = $this->input->post("campaign_id");
		$campaign_input = $this->input->post("campaignData");
		$this->load->model("campaign_model");
		$output = $this->campaign_model->campaign_update($campaign_id,$campaign_input);
		echo $output;
	}
	public function delete_campaign()
	{
		$campaign_id = $this->input->post("campaign_id");
		$this->load->model("campaign_model");
		$this->campaign_model->delete_row($campaign_id);
	}
	public function social_test()
	{
		$this->load->model("social_model");
		//echo $this->social_model->get_tweets("http://www.google.com/");
		echo $this->social_model->get_fb("http://www.google.com/");
		//echo $this->social_model->get_plusones("http://www.google.com/");
		//echo $this->social_model->get_stumble("http://www.google.com/");
		//echo $this->social_model->get_pinterest("http://www.google.com/");
	}
	public function share_campaign($campaign_id = NULL)
	{
		$this->load->model("campaign_model");
		$campaign_array = $this->campaign_model->get_row($campaign_id);
		$output = $this->load->view("marketer/campaign_share",$campaign_array,TRUE);
		echo $output;
	}
	
}

